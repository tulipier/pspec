from django.forms.widgets import ChoiceWidget, Select


class BSHierarchySelect(ChoiceWidget):
    allow_multiple_selected = False
    input_type = "input"
    template_name = "bootstrap_hierarchy_select/widgets/hierarchy_select.html"
    option_template_name = "bootstrap_hierarchy_select/widgets/hierarchy_select_option.html"
    checked_attribute = {"data-default-selected": ""}

    add_depth_to_choices = None

    class Media:
        css = {
            'all': ('css/hierarchy-select/hierarchy-select.min.css',)
        }
        js = ('js/hierarchy-select/hierarchy-select.min.js',)


    @property
    def choices(self):
        if self._choices_depth is None:
            self._choices, self._choices_depth = self.add_depth_to_choices(self._choices)
        return self._choices

    @choices.setter
    def choices(self, val):
        self._choices_depth = None
        self._choices = val
        # if self.add_depth_to_choices is not None:
        #     self._choices, self._choices_depth = self.add_depth_to_choices(val)
        # else:
        #     self._choices = val
        #     self._choices_depth = None
        # print("Plip")

    def __init__(self, attrs=None, choices=(), depth_choices_fnct=None):
        super().__init__(attrs)
        # choices can be any iterable, but we may need to render this widget
        # multiple times. Thus, collapse it into a list so it can be consumed
        # more than once.
        self.add_depth_to_choices = depth_choices_fnct
        self._choices_depth = None
        self.choices = list(choices)

    def get_context(self, name: str, value, attrs):
        context = super(BSHierarchySelect, self).get_context(name, value, attrs)
        if self.choices is not None:
            context['widget']['selected_label'] = [elem[1] for elem in self.choices if elem[0] == value][0]
        return context


    def create_option(
        self, name, value, label, selected, index, subindex=None, attrs=None
    ):
        option = super(BSHierarchySelect, self).create_option(name, value, label, selected, index, subindex, attrs)
        option['attrs']["data-level"] = self._choices_depth[int(index)]
        return option
