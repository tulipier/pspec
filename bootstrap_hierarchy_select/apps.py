from django.apps import AppConfig


class BootstrapHierarchySelectConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "bootstrap_hierarchy_select"
