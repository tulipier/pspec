from django.http.response import JsonResponse
from django.db import transaction, Error
from django.http.response import HttpResponseBadRequest, HttpResponse

from rest_framework.views import APIView

from solution.tree import SolutionTree

from specification.models import Project, Specification

from solution.models import SolutionElement, SolutionSet


class SolutionApi(APIView):
    def get(self, request, format=None):
        solution_set = SolutionSet.objects.none()
        solution_set_id = request.session.get("current_solution_id", False)
        if solution_set_id is not False:
            solution_set = SolutionSet.objects.get(pk=solution_set_id)
        solutions = SolutionElement.objects.filter(solution=solution_set)
        parser = SolutionTree(solutions, request.session["current_project_id"], solution_set_id)
        project = Project.objects.get(pk=request.session["current_project_id"])
        parser.add_spec_tree(Specification.objects.filter(project__pk=project.pk))
        # parser.add_gain_to_graph()
        gain = parser.compute_gain_for_graph()
        tree = parser.get_nested_tree()
        # tree[0]["title"] = f"—— {project.name} ——"
        return JsonResponse({'tree': tree, 'gain': gain}, safe=False)


class SolutionElementMoveApi(APIView):
    def post(self, request, solution_elem_id):
        try:
            with transaction.atomic():
                solution_set_id = request.session["current_solution_id"]
                solution = SolutionElement.objects.get(pk=solution_elem_id, solution__pk=solution_set_id)
                if request.POST.get('from', '') != '':
                    solution_from = SolutionElement.objects.get(pk=request.POST.get('from'), solution__pk=solution_set_id)
                    solution.included_in.remove(solution_from)
                if request.POST.get('to', '') != '':
                    solution_to = SolutionElement.objects.get(pk=request.POST.get('to'), solution__pk=solution_set_id)
                    solution.included_in.add(solution_to)
            return SolutionApi().get(request)
        except Error:
            return HttpResponseBadRequest()


class SolutionActivateApi(APIView):
    def get(self, request, solution_set_id):
        try:
            solution_set = SolutionSet.objects.get(pk=solution_set_id)
            solution_set.set_active_for_session(request.session)
            return HttpResponse()
        except SolutionSet.DoesNotExist:
            return HttpResponseBadRequest()
