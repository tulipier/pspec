��    
      l      �       �   )   �           3  )   K     u     z     �     �     �  A  �  5     "   G  !   j  +   �     �     �     �      �                     
                  	           Add solution element included in this one Close all sub-solutions Delete solution element Delete this solution element (everywhere) Edit Edit solution element New solution element Open all sub-solutions Visualize solution Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Ajouter un élément de solution inclus dans celui-ci Refermer toutes les sous-solutions Supprimer l'élément de solution Supprimer l'élément de solution (partout) Éditer Éditer l'élément de solution Nouvel élément de solution Ouvrir toutes les sous-solutions Visualiser les solutions 