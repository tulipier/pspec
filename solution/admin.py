from typing import Union

from django.contrib import admin
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.shortcuts import render, redirect
from django import forms

from django.http.request import HttpRequest

from solution.models import SolutionSet, SolutionElement, Fulfillment, Breach

from django.db.models.query import QuerySet

from django.utils.translation import gettext_lazy as _


# Register your models here.

@admin.register(SolutionSet)
class SolutionSetAdmin(admin.ModelAdmin):
    actions = ['action_activate', ]

    @admin.action(description=_("Activate this solution"))
    def action_activate(self, request: HttpRequest, queryset: 'QuerySet[SolutionSet]'):
        if len(queryset) != 1:
            raise ValueError(_("Should select one and only one solution to activate"))
        queryset[0].set_active_for_session(request.session)
        return redirect(request.get_full_path())


class SolutionElementAdminForm(forms.ModelForm):
    class Meta:
        model = SolutionElement
        widgets = {
            'included_in': FilteredSelectMultiple('included_in', False),
        }
        fields = '__all__'


class FulfillmentInline(admin.TabularInline):
    model = Fulfillment
    extra = 1

    def formfield_for_foreignkey(self, db_field, request: 'Union[None, HttpRequest]' = None, **kwargs):
        field = super(FulfillmentInline, self).formfield_for_foreignkey(db_field, request, **kwargs)

        if db_field.name == 'specification':
            field.queryset = field.queryset.filter(project_id=request.session['current_project_id'])
        elif db_field.name == 'solution_element':
            field.queryset = field.queryset.filter(solution_id=request.session['current_solution_id'])

        return field


class BreachInline(admin.TabularInline):
    model = Breach
    extra = 1

    def formfield_for_foreignkey(self, db_field, request: 'Union[None, HttpRequest]' = None, **kwargs):
        field = super(BreachInline, self).formfield_for_foreignkey(db_field, request, **kwargs)

        if db_field.name == 'specification':
            field.queryset = field.queryset.filter(project_id=request.session['current_project_id'])
        elif db_field.name == 'solution_element':
            field.queryset = field.queryset.filter(solution_id=request.session['current_solution_id'])

        return field


@admin.register(SolutionElement)
class SolutionElementAdmin(admin.ModelAdmin):
    form = SolutionElementAdminForm
    inlines = (FulfillmentInline, BreachInline)

    def get_queryset(self, request):
        current_solution_id = request.session.get("current_solution_id", False)
        qs = SolutionElement.objects.none()
        if current_solution_id is not False:
            qs = SolutionElement.objects.filter(solution_id=current_solution_id)
        return qs
