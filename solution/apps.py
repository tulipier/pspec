from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class SolutionConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "solution"
    verbose_name = _("solution")
