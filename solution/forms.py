from django.db import transaction, Error
from django.forms.widgets import Textarea
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.forms import ModelForm, inlineformset_factory
from django import forms
from django.utils.translation import gettext_lazy as _

from django.http.response import HttpResponseBadRequest

from solution.models import Fulfillment, Breach, SolutionElement

import specification.forms
from specification.models import Specification

from bootstrap_hierarchy_select.widgets import BSHierarchySelect

from specification.tree import add_depth_to_graph


class FulfillmentForm(ModelForm):
    class Meta:
        model = Fulfillment
        fields = '__all__'
        widgets = {
            'details': Textarea(attrs={
                'rows': 1
            }),
            'specification': BSHierarchySelect(depth_choices_fnct=add_depth_to_graph),
            'solution_element': BSHierarchySelect(depth_choices_fnct=add_depth_to_graph)
        }
        # exclude = ['details', ]


class BreachForm(ModelForm):
    class Meta:
        model = Breach
        fields = '__all__'
        widgets = {
            'details': Textarea(attrs={
                'rows': 1
            }),
            'specification': BSHierarchySelect(depth_choices_fnct=add_depth_to_graph),
            'solution_element': BSHierarchySelect(depth_choices_fnct=add_depth_to_graph)
        }
        # exclude = ['details', ]


class PspecHTMLFormMixin(specification.forms.PspecHTMLFormMixin):
    def filter_solution_element(self):
        """
        Allow to filter solution element by current solution set
        :return:
        """
        return SolutionElement.objects.filter(solution_id=self.request.session['current_solution_id'])


class PspecHTMLCRUDMixin(PspecHTMLFormMixin, specification.forms.PspecHTMLCRUDMixin):
    pass


class SolutionElementMixin:
    template_name_suffix = "_create"

    def get_context_data(self, **kwargs):
        context = super(SolutionElementMixin, self).get_context_data(**kwargs)
        fulfillment_formset = inlineformset_factory(
            SolutionElement,
            Fulfillment,
            form=FulfillmentForm,
            extra=1,
            can_delete=True,
            # formfield_callback=FulfillmentForm.formfield_for_foreignkey
        )
        fulfillment_formset.form.base_fields['specification'].queryset = self.filter_project_specification()
        if self.request.POST:
            context['fulfillment_formset'] = fulfillment_formset(self.request.POST, instance=self.object)
        else:
            context['fulfillment_formset'] = fulfillment_formset(instance=self.object)
        breach_formset = inlineformset_factory(
            SolutionElement,
            Breach,
            form=BreachForm,
            extra=1,
            can_delete=True,
            # formfield_callback=FulfillmentForm.formfield_for_foreignkey
        )
        breach_formset.form.base_fields['specification'].queryset = self.filter_project_specification()
        if self.request.POST:
            context['breach_formset'] = breach_formset(self.request.POST, instance=self.object)
        else:
            context['breach_formset'] = breach_formset(instance=self.object)
        return context

    def form_valid(self, form):
        context = self.get_context_data()
        fulfillment_formset = context['fulfillment_formset']
        breach_formset = context['breach_formset']
        try:
            with transaction.atomic():
                ret = super(SolutionElementMixin, self).form_valid(form)

                if fulfillment_formset.is_valid():
                    fulfillment_formset.instance = self.object
                    fulfillment_formset.save()
                    if breach_formset.is_valid():
                        breach_formset.instance = self.object
                        breach_formset.save()
                        return ret
                else:
                    raise Error
        except Error:
            return HttpResponseBadRequest()


class ManageSolutionElementMixin(PspecHTMLCRUDMixin):
    template_name_suffix = "_manage_solutions"

    def get_context_data(self, **kwargs):
        context = super(ManageSolutionElementMixin, self).get_context_data(**kwargs)
        fulfillment_formset = inlineformset_factory(
            Specification,
            Fulfillment,
            form=FulfillmentForm,
            extra=1,
            can_delete=True
        )
        fulfillment_formset.form.base_fields['solution_element'].queryset = self.filter_solution_element()
        if self.request.POST:
            context['fulfillment_formset'] = fulfillment_formset(self.request.POST, instance=self.object)
        else:
            context['fulfillment_formset'] = fulfillment_formset(instance=self.object)
        breach_formset = inlineformset_factory(
            Specification,
            Breach,
            form=BreachForm,
            extra=1,
            can_delete=True
        )
        breach_formset.form.base_fields['solution_element'].queryset = self.filter_solution_element()
        if self.request.POST:
            context['breach_formset'] = breach_formset(self.request.POST, instance=self.object)
        else:
            context['breach_formset'] = breach_formset(instance=self.object)
        return context

    def form_valid(self, form):
        context = self.get_context_data()
        fulfillment_formset = context['fulfillment_formset']
        breach_formset = context['breach_formset']
        try:
            with transaction.atomic():
                ret = super(ManageSolutionElementMixin, self).form_valid(form)

                if fulfillment_formset.is_valid():
                    fulfillment_formset.instance = self.object
                    fulfillment_formset.save()
                    if breach_formset.is_valid():
                        breach_formset.instance = self.object
                        breach_formset.save()
                        return ret
                else:
                    raise Error
        except Error:
            return HttpResponseBadRequest()

    def get_post_url(self):
        return f"{self.post_url}{self.object.pk}"


class VisualizeSolutionForm(forms.Form):
    solutions = forms.ModelMultipleChoiceField(
        queryset=SolutionElement.objects.all(),
        required=True,
        widget=FilteredSelectMultiple('solutions', False)
    )  # will be override in __init__
    visualize_mode = forms.ChoiceField(choices=(
        ('mindmap', _("PlantUML MindMap")),
        # ('deploy', "PlantUML Deploy"),
        # ('deploy_nested', "PlantUML Deploy (Nested)"),
        # ('wbs', "PlantUML WBS"),
    ))
