from django.http.response import HttpResponseRedirect, HttpResponseBadRequest
from django.http.request import HttpRequest
from django.shortcuts import render
from django.utils.translation import gettext_lazy as _

from django.views.generic.base import View, TemplateView
from django.views.generic.edit import DeleteView, UpdateView, CreateView, FormView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.forms.widgets import HiddenInput

from solution.models import SolutionSet, SolutionElement
from solution.forms import PspecHTMLFormMixin, PspecHTMLCRUDMixin, SolutionElementMixin, VisualizeSolutionForm


import specification.views

import plantweb.render


class SolutionView(LoginRequiredMixin, specification.views.SiteViewMixin, TemplateView):
    page = "solution"
    template_name = "solution/solution.html"


class SolutionActivateView(View):
    def get(self, request: HttpRequest, solution_set_id):
        try:
            solution = SolutionSet.objects.get(pk=solution_set_id)
            solution.set_active_for_session(request.session)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
        except SolutionSet.DoesNotExist:
            return HttpResponseBadRequest()


class SolutionElementEditView(SolutionElementMixin, PspecHTMLCRUDMixin, UpdateView):
    model = SolutionElement
    fields = [
        'title',
        'description',
        'included_in'
    ]
    widgets = {
        'included_in': FilteredSelectMultiple(_('parent solutions'), False)
    }
    querysets = {
        'included_in': PspecHTMLFormMixin.filter_solution_element
    }

    def get_post_url(self):
        return f"/api/html/edit/{ self.model_name }/{ self.object.pk }"


class SolutionElementDeleteView(PspecHTMLCRUDMixin, DeleteView):
    model = SolutionElement

    def get_post_url(self):
        return f"/api/html/delete/{ self.model_name }/{ self.object.pk }"


class SolutionElementCreateView(SolutionElementMixin, PspecHTMLCRUDMixin, CreateView):
    model = SolutionElement
    post_url = "/api/html/create/solutionelement/"
    fields = [
        'solution',
        'title',
        'description',
        # 'fulfill',
        # 'breach',
        'included_in'
    ]
    widgets = {
        'solution': HiddenInput(),
        'included_in': FilteredSelectMultiple(_('parent solutions'), False)
    }
    querysets = {
        'included_in': PspecHTMLFormMixin.filter_solution_element
    }

    def get_initial(self):
        initial = super(SolutionElementCreateView, self).get_initial()
        initial['solution'] = self.request.session['current_solution_id']
        return initial


class SolutionElementVisualizeView(PspecHTMLFormMixin, FormView):
    template_name = "specification/specification_form.html"
    form_class = VisualizeSolutionForm
    post_url = "/api/html/visualize/solutionelement/"
    querysets = {
        'solutions': PspecHTMLFormMixin.filter_solution_element
    }

    def form_valid(self, form: VisualizeSolutionForm):
        visu_class = None
        if form.cleaned_data['visualize_mode'] == "mindmap":
            visu_class = specification.specviz.PlantUMLVizMindmap
        elif form.cleaned_data['visualize_mode'] == "deploy":
            visu_class = specification.specviz.PlantUMLVizDeploy
        elif form.cleaned_data['visualize_mode'] == "deploy_nested":
            visu_class = specification.specviz.PlantUMLVizDeployNested
        elif form.cleaned_data['visualize_mode'] == "wbs":
            visu_class = specification.specviz.PlantUMLVizWBS

        solutions = form.cleaned_data['solutions']
        visu = visu_class(solutions)
        plantuml = visu.generate_plantuml_content()
        graph = plantweb.render.render(plantuml, engine="plantuml")
        graph = graph[0].decode("utf8")
        return render(self.request, "specification/visualize.html", context={
            "graph": graph,
            "plantuml": plantuml
        })
