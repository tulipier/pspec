function start_edit_solution(solution_id) {
    xhr_form_on_left_panel(_("Edit solution element"), {
        type: "GET",
        url: "/api/html/edit/solutionelement/" + solution_id,
    }, null, () => {
        close_left_panel();
        // $.ui.fancytree.getTree("#tree").reload()
        reload_tree_keep_scroll_position($.ui.fancytree.getTree("#tree"));
    });
}

function start_create_solution(solution_id) {
    xhr_form_on_left_panel(_("New solution element"), {
        type: "GET",
        url: "/api/html/create/solutionelement/",
    }, () => {
        if (solution_id !== undefined && solution_id !== null) {
            document.querySelector('#id_included_in_from option[value="' + solution_id + '"]').selected = true;
            SelectBox.move('id_included_in_from', 'id_included_in_to');
        }
    }, () => {
        close_left_panel();
        // $.ui.fancytree.getTree("#tree").reload()
        reload_tree_keep_scroll_position($.ui.fancytree.getTree("#tree"));
        // TODO
        // Expand spec_id node
    });
}

function start_delete_solution(solution_id) {
    xhr_form_on_left_panel(_("Delete solution element"), {
        type: "GET",
        url: "/api/html/delete/solutionelement/" + solution_id,
    }, null, () => {
        close_left_panel();
        // $.ui.fancytree.getTree("#tree").reload()
        reload_tree_keep_scroll_position($.ui.fancytree.getTree("#tree"));
    });
}

function start_visualize_solutions() {
    // let selected = "";
    // $.ui.fancytree.getTree("#tree").visit(function (node) {
    //     if (node.isSelected()) {
    //         selected += node.refKey + ",";
    //     }
    // });
    xhr_form_on_left_panel(_("Visualize solution"), {
        type: "GET",
        url: "/api/html/visualize/solutionelement/", // ?specs="+selected,
        // TODO
        // Replace js selection by html selection
    }, () => {
        // // Add via js selected (checkbox)
        let from_select = document.getElementById('id_solutions_from');
        $.ui.fancytree.getTree("#tree").visit(function (node) {
            if (node.isSelected()) {
                from_select.querySelector("#id_solutions_from option[value='" + node.refKey + "']").selected = true;
            }
        });
        SelectBox.move('id_solutions_from', 'id_solutions_to');
    });

    /*
    // Show in next tab
    let process_response = function (evt, detail) {
        console.log(evt);
        if(evt.detail.successful){
            // close_left_panel();
            console.log("Open new window !");
            let newTab = window.open();
            newTab.document.body.innerHTML = evt.detail.xhr.response;
            console.log(evt)
        }
        // TODO
        // Display error ?
        container.removeEventListener('htmx:afterRequest', process_response);
    };
    container.addEventListener('htmx:afterRequest', process_response);
     */
}

$(function () {
    move_url = "/api/solution/solutionelement/move/";
    fancytree_config.source.url = "/api/solution/tree";
    // fancytree_config.renderColumns = function (event, data) {
    //     let node = data.node
    //     let $tdList = $(node.tr).find(">td");
    //     $tdList.eq(2).text(node.data['gain'].toFixed(0));
    // }
    fancytree_config.postProcess = function(event, data) {
        // Get the 'tree' object from AJAX source
        data.result = data.response.tree;
        console.log(data.response.gain)
        $('#treetable_total_gain').text(data.response.gain.toFixed(0));
    };
    fancytree_config.renderColumns = function (event, data) {
        let node = data.node
        let $tdList = $(node.tr).find(">td");
        $tdList.eq(2).html(() => {
            // Display fulfill/breach column
            let val = '';
            node.data['fulfill'].forEach((elem)=>{
                val += "<span class='badge text-bg-primary'>+"+elem[0]+"% "+elem[1]+"</span>";
            })
            node.data['breach'].forEach((elem)=>{
                val += "<span class='badge text-bg-danger'>-"+elem[0]+"% "+elem[1]+"</span>";
            })
            return val;
        });
    }
    fancytree_config.treeId = "solution";
    fix_fancytree_persist_storage(fancytree_config.treeId);
    $("#tree")
        .fancytree(fancytree_config);

    contextmenu_config.items = {
        "edit": {
            name: _("Edit"),
            icon: "edit",
            callback: function (key, opt) {
                let node = $.ui.fancytree.getNode(opt.$trigger);
                start_edit_solution(node.refKey)

                // window.open("/admin/specification/specification/" + node.refKey + "/change/", '_blank').focus();
            }
        },
        "add": {
            name: _("Add solution element included in this one"),
            icon: "add",
            callback: function (key, opt) {
                let node = $.ui.fancytree.getNode(opt.$trigger);
                start_create_solution(node.refKey);
            }
        },
        "delete": {
            name: _("Delete this solution element (everywhere)"),
            icon: "delete",
            callback: function (key, opt) {
                let node = $.ui.fancytree.getNode(opt.$trigger);
                start_delete_solution(node.refKey);
            }
        },
        "sep": "----",
        ...contextmenu_config.items,
    }
    contextmenu_config.items.open_all.name = _("Open all sub-solutions");
    contextmenu_config.items.close_all.name = _("Close all sub-solutions");
    $.contextMenu(contextmenu_config);

    $("input[name=search]").on("keyup", function (e) {
            // if(e.key === "Backspace" && e.target.value.length > 0){
            //     e.target.value = e.target.value.substring(0, e.target.value.length - 1);
            // }else if(e.key === " "){
            //     e.target.value += ' ';
            // }
            let tree = $.ui.fancytree.getTree();
            let match = $(this).val();

            if (match === "") {
                tree.clearFilter();
            } else {
                tree.filterNodes(match);
            }

            // Start to collapse parents that aren't needed from old matches
            tree.visit((node) => {
                if (!node.match) {
                    node.visitParents((parent) => {
                        if (parent.isExpanded() && parent._filterAutoExpandedFixed) {
                            parent.setExpanded(false);
                            delete parent._filterAutoExpandedFixed;
                        }
                    });
                }
            });
            // Expand all parent from all matches
            tree.visit((node) => {
                if (node.match) {
                    node.visitParents((parent) => {
                        if (!parent.isExpanded()) {
                            parent.setExpanded(true);
                            parent._filterAutoExpandedFixed = true;
                        }
                    });
                }
            });
        }).focus();

});