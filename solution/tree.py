from specification.tree import FancyTree, SpecificationTree
from solution.models import SolutionElement, Fulfillment, Breach
import networkx


class SolutionTree(FancyTree):
    specs_tree = None
    specs_graph = None
    gains = None
    child_gains = None
    total_gain = None

    def __init__(self, nodes, project_id, solution_id):
        super(SolutionTree, self).__init__(nodes, project_id)
        self.solution_id = solution_id

    def get_queryset(self,
                     nodes: 'QuerySet[SolutionElement]') -> 'QuerySet[SolutionElement]':
        qs = super(SolutionTree, self).get_queryset(nodes)
        self.linked_fulfillment = dict((o.pk, list()) for o in qs)
        self.linked_breach = dict((o.pk, list()) for o in qs)
        for node in Fulfillment.objects.filter(solution_element__in=qs).prefetch_related('specification'):  # type: Fulfillment
            self.linked_fulfillment[node.solution_element_id].append((node.percent, node.specification.title))
        for node in Breach.objects.filter(solution_element__in=qs).prefetch_related('specification'):  # type: Breach
            self.linked_breach[node.solution_element_id].append((node.percent, node.specification.title))

        return qs

    def add_spec_tree(self, specs):
        self.specs_graph = SpecificationTree(specs, self.project_id)
        self.specs_graph.add_cost_to_graph()
        self.specs_tree = self.specs_graph.get_nested_tree()
        self.specs_graph.add_fulfillment_to_tree(self.specs_tree, self.solution_id)
        # self.specs_graph.add_cost_to_graph()

    def compute_gain_for_graph(self):
        gain = 0
        # for spec_without_parent in [n for n in self.specs_graph.graph.nodes() if self.specs_graph.graph.in_degree(n) == 0]:
        #     gain += (self.specs_graph.fulfilment[spec_without_parent]/100.0) * self.specs_graph.costs[spec_without_parent]
        for spec_id, spec in self.specs_graph.db_nodes.items():
            gain += (self.specs_graph.fulfilment_intra[spec_id]/100.0)*self.specs_graph.costs[spec_id]
        return gain

    # def add_gain_to_graph(self):
    #     self.gains = dict()
    #     nodes_without_parents = [n for n in self.graph.nodes() if self.graph.in_degree(n) == 0]
    #     # Parse the graph in the BFS order
    #     for node in list(networkx.topological_sort(self.graph))[::-1]:
    #         self._compute_solution_gain(node, root=node in nodes_without_parents)
    #     for node in list(networkx.topological_sort(self.graph))[::-1]:
    #         self._compute_solution_child_gain(node, root=node in nodes_without_parents)
    #     self.total_gain = sum(self.gains.values())
    #
    # def _compute_solution_gain_node(self, solution: SolutionElement):
    #     gain = 0
    #     for spec_node in solution.fulfill.all():
    #         gain += self.specs_graph.costs[spec_node.pk]
    #     return gain
    #
    # def _compute_solution_gain(self, node, root=False):
    #     solution = self.graph.nodes[node]['data'] # type: SolutionElement
    #     # if root:
    #     #     # Node without parent
    #     #     self.resolutions[node] = self._compute_solution_resolution_node(solution)
    #     # else:
    #     if node not in self.gains.keys():
    #         self.gains[node] = self._compute_solution_gain_node(solution)
    #     # for child in solution.solutionelement_set.all():
    #     #     self.gains[node] += self.gains[child.pk] / child.included_in.count()
    #
    # # def _compute_solution_child_gain(self, node):
    # #     solution = self.graph.nodes[node]['data']
    # #     for child in solution.solutionelement_set.all():
    # #         self.gains[node] += self.gains[child.pk] / child.included_in.count()
    #
    def _get_dict_from_node(self, node: 'SolutionElement'):
        node_dict = super(SolutionTree, self)._get_dict_from_node(node)
        node_dict['fulfill'] = self.linked_fulfillment[node.pk]
        node_dict['breach'] = self.linked_breach[node.pk]
        return node_dict