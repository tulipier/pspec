from django.db import models
from django.utils.translation import gettext_lazy as _

from specification.models import Specification, Project


class SolutionSet(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    name = models.CharField(max_length=256)

    class Meta:
        verbose_name = _("solution set")
        verbose_name_plural = _("solution sets")

    def set_active_for_session(self, session):
        session['current_solution_id'] = self.pk
        session['current_solution_name'] = self.name

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name


class SolutionElement(models.Model):
    solution = models.ForeignKey(SolutionSet, verbose_name=_("solution"), on_delete=models.CASCADE)  # Many2Many ?
    title = models.CharField(max_length=256, verbose_name=_("title"), )
    description = models.TextField(blank=True, verbose_name=_("description"))
    included_in = models.ManyToManyField('solution.SolutionElement', blank=True, verbose_name=_("included in"))
    fulfill = models.ManyToManyField(Specification, through='solution.Fulfillment', related_name="fulfillment+",
                                     blank=True, verbose_name=_("fulfill"))
    breach = models.ManyToManyField(Specification, through='solution.Breach', related_name="breach+", blank=True,
                                    verbose_name=_("breach"))
    cost = models.FloatField(default=0.0, verbose_name=_("cost"))

    class Meta:
        verbose_name = _("solution element")
        verbose_name_plural = _("solution elements")

    def __str__(self):
        return self.title

    def __repr__(self):
        return self.title


class Fulfillment(models.Model):
    specification = models.ForeignKey('specification.Specification', on_delete=models.CASCADE,
                                      verbose_name=_("specification"))
    solution_element = models.ForeignKey('solution.SolutionElement', on_delete=models.CASCADE,
                                         related_name="fullfil+", verbose_name=_("solution element"))
    percent = models.FloatField(default=0.0, verbose_name=_("percent"))
    details = models.TextField(blank=True, verbose_name=_("details"))

    class Meta:
        unique_together = (('specification', 'solution_element'),)
        verbose_name = _("fulfillment")


class Breach(models.Model):
    specification = models.ForeignKey('specification.Specification', on_delete=models.CASCADE,
                                      verbose_name=_("specification"))
    solution_element = models.ForeignKey('solution.SolutionElement', on_delete=models.CASCADE,
                                         related_name="breach+", verbose_name=_("solution element"))
    percent = models.FloatField(default=0.0, verbose_name=_("percent"))
    details = models.TextField(blank=True, verbose_name=_("details"))

    class Meta:
        unique_together = (('specification', 'solution_element'),)
        verbose_name = _("breach")
