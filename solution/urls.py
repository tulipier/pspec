from django.urls import path

import solution.views
import solution.api

urlpatterns = [
    path("solution", solution.views.SolutionView.as_view()),
    path("solution/activate/<int:solution_set_id>", solution.views.SolutionActivateView.as_view()),
]


# HTMX
urlpatterns += [
    path("api/html/edit/solutionelement/<int:pk>", solution.views.SolutionElementEditView.as_view()),
    path("api/html/create/solutionelement/", solution.views.SolutionElementCreateView.as_view()),
    path("api/html/delete/solutionelement/<int:pk>", solution.views.SolutionElementDeleteView.as_view()),
    path("api/html/visualize/solutionelement/", solution.views.SolutionElementVisualizeView.as_view()),
]

# API
urlpatterns += [
    path("api/solution/tree", solution.api.SolutionApi.as_view()),
    path("api/solution/solutionelement/move/<int:solution_elem_id>", solution.api.SolutionElementMoveApi.as_view()),
    path("api/solution/solution/activate/<int:solution_id>", solution.api.SolutionActivateApi.as_view()),
]
