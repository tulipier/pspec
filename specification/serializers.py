from rest_framework import serializers

from specification.models import Specification


class SpecificationSerializerMinimal(serializers.ModelSerializer):
    class Meta:
        model = Specification
        fields = ['id', 'title', 'included_in']


# class SpecificationTreeSerializer(serializers.Serializer):
#     id = serializers.IntegerField(read_only=True)
#     title = Specification.title
#     code = serializers.CharField(style={'base_template': 'textarea.html'})