from django.urls import path

import specification.views
import specification.api

urlpatterns = [
    path("admin/specification/visualize", specification.views.VisualizeSpecificationView.as_view()),
    path("test", specification.views.TestView.as_view()),
    path("test_scratch", specification.views.TestScratchView.as_view()),
    path("treetable", specification.views.TreeTableView.as_view()),
    path("", specification.views.LoginView.as_view()),
    path("project/activate/<int:project_id>", specification.views.ProjectActivateView.as_view()),
]

# Account
urlpatterns += [
    path("accounts/login/", specification.views.LoginView.as_view()),
    path("accounts/logout/", specification.views.LogoutView.as_view()),
    path("accounts/password_change/", specification.views.ChangePasswordView.as_view()),
]

# HTMX
urlpatterns += [
    path("api/html/edit/specification/<int:pk>", specification.views.SpecificationEditView.as_view()),
    path("api/html/edit_relations/specification/<int:pk>", specification.views.SpecificationEditRelationsView.as_view()),
    path("api/html/create/specification/", specification.views.SpecificationCreateView.as_view()),
    path("api/html/delete/specification/<int:pk>", specification.views.SpecificationDeleteView.as_view()),
    path("api/html/tooltip/specification/<int:pk>", specification.views.SpecificationTooltipView.as_view()),
    path("api/html/visualize/specification/", specification.views.SpecificationVisualizeView.as_view()),
    path("api/html/manage_solutions/specification/<int:pk>",
         specification.views.ManageSolutionsForSpecificationView.as_view()),
    path("api/html/accounts/password_change/", specification.views.ChangePasswordApiView.as_view()),
]

# API
urlpatterns += [
    path("api/specification/tree", specification.api.SpecificationApi.as_view()),
    path("api/specification/spec/move/<int:spec_id>", specification.api.SpecificationMoveApi.as_view()),
    path("api/specification/supervisor/toggle/<int:spec_id>/<int:supervisor_id>", specification.api.ToggleSupervisorAPI.as_view()),
    path("api/specification/tag/toggle/<int:spec_id>/<int:tag_id>", specification.api.ToggleTagAPI.as_view()),
    path("api/specification/concerned/toggle/<int:spec_id>/<int:actor_id>", specification.api.ToggleConcernedAPI.as_view()),
    path("api/specification/project/activate/<int:project_id>", specification.api.ProjectActivateApi.as_view()),
]
