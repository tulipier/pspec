let pspec_tags = {};
let pspec_supervisors = {};
let pspec_actors = {};
let pspec_filter_title = null;
let pspec_filter_tags = null;
let pspec_filter_supervisors = null;
let pspec_filter_concerned = null;
let pspec_auto_reload = true;
let pspec_auto_reload_pending = 0;

function render_tag(tag_id){
    return "<span class='badge' style='color: "+pspec_tags[tag_id].text_color+
        "; background-color: "+pspec_tags[tag_id].color+"'>"+pspec_tags[tag_id].name+"</span>";
}

function toggle_auto_reload(){
    pspec_auto_reload = !pspec_auto_reload;
    if(pspec_auto_reload === true){
        document.getElementById('auto_reload_icon').className = 'bi-cloud-download-fill';
        if (pspec_auto_reload_pending > 0){
            auto_reload_tree();
            pspec_auto_reload_pending = 0;
            document.getElementById('auto_reload_badge').innerText = "";
        }
    }else{
        document.getElementById('auto_reload_icon').className = 'bi-cloud';
    }
}

function toggle_select_all_tree(checkbox) {
    if (checkbox.checked !== true) {
        checkbox.checked = true
        checkbox.classList.add("fancytree-selected");
    } else {
        checkbox.checked = false;
        checkbox.classList.remove("fancytree-selected");
    }
    $.ui.fancytree.getTree("#tree").visit(function (node) {
        node.setSelected(checkbox.checked);
    });
}

function auto_reload_tree(){
    if(pspec_auto_reload === true){
        // $.ui.fancytree.getTree("#tree").reload()
        reload_tree_keep_scroll_position($.ui.fancytree.getTree("#tree"));
    }else{
        pspec_auto_reload_pending += 1;
        document.getElementById('auto_reload_badge').innerText = pspec_auto_reload_pending.toString();
    }
}

function start_edit_spec(spec_id) {
    xhr_form_on_left_panel(_("Edit spec"), {
        type: "GET",
        url: "/api/html/edit/specification/" + spec_id,
    }, null, () => {
        close_left_panel();
        auto_reload_tree();
    });
}

function start_edit_spec_relations(spec_id) {
    xhr_form_on_left_panel(_("Edit spec relations"), {
        type: "GET",
        url: "/api/html/edit_relations/specification/" + spec_id,
    }, null, () => {
        close_left_panel();
        auto_reload_tree();
    });
}

function start_edit_spec_solutions(spec_id) {
    xhr_form_on_left_panel(_("Manage solutions"), {
        type: "GET",
        url: "/api/html/manage_solutions/specification/" + spec_id,
    }, null, () => {
        close_left_panel();
        auto_reload_tree();
    });
}

function start_create_spec(spec_id) {
    xhr_form_on_left_panel(_("New spec"), {
        type: "GET",
        url: "/api/html/create/specification/",
    }, () => {
        if (spec_id !== undefined && spec_id !== null) {
            // Init hierarchySelect and then set select parent spec
            let elem = $('#specificationincludedin_set-0-to_specification');
            elem.hierarchySelect({width: '100%'})
            elem.data().HierarchySelect.setValue(spec_id)
        }
    }, () => {
        close_left_panel();
        auto_reload_tree();
        // TODO
        // Expand spec_id node
    });
}

function start_delete_spec(spec_id) {
    xhr_form_on_left_panel(_("Delete spec"), {
        type: "GET",
        url: "/api/html/delete/specification/" + spec_id,
    }, null, () => {
        close_left_panel();
        auto_reload_tree();
    });
}

function start_visualize_specs() {
    // let selected = "";
    // $.ui.fancytree.getTree("#tree").visit(function (node) {
    //     if (node.isSelected()) {
    //         selected += node.refKey + ",";
    //     }
    // });
    xhr_form_on_left_panel(_("Visualize specs"), {
        type: "GET",
        url: "/api/html/visualize/specification/", // ?specs="+selected,
        // TODO
        // Replace js selection by html selection
    }, () => {
        // // Add via js selected (checkbox)
        let from_select = document.getElementById('id_specs_from');
        $.ui.fancytree.getTree("#tree").visit(function (node) {
            if (node.isSelected()) {
                from_select.querySelector("#id_specs_from option[value='" + node.refKey + "']").selected = true;
            }
        });
        SelectBox.move('id_specs_from', 'id_specs_to');
    });

    /*
    // Show in next tab
    let process_response = function (evt, detail) {
        console.log(evt);
        if(evt.detail.successful){
            // close_left_panel();
            console.log("Open new window !");
            let newTab = window.open();
            newTab.document.body.innerHTML = evt.detail.xhr.response;
            console.log(evt)
        }
        // TODO
        // Display error ?
        container.removeEventListener('htmx:afterRequest', process_response);
    };
    container.addEventListener('htmx:afterRequest', process_response);
     */
}

function toggle_tag(spec_id, tag_id){
    $.ajax({
        type: "GET",
        url: "/api/specification/tag/toggle/" + spec_id + "/" + tag_id,
    }).done(()=>{
        auto_reload_tree();
    });
}

function toggle_concerned(spec_id, actor_id){
    $.ajax({
        type: "GET",
        url: "/api/specification/concerned/toggle/" + spec_id + "/" + actor_id,
    }).done(()=>{
        auto_reload_tree();
    });
}

function toggle_supervisor(spec_id, supervisor_id){
    $.ajax({
        type: "GET",
        url: "/api/specification/supervisor/toggle/" + spec_id + "/" + supervisor_id,
    }).done(()=>{
        auto_reload_tree();
    });
}


$(function () {
        move_url = "/api/specification/spec/move/";
        fancytree_config.source.url = "/api/specification/tree";
        fancytree_config.renderColumns = function (event, data) {
            let node = data.node
            let $tdList = $(node.tr).find(">td");
            if (data.node.tr) { // Try to init tooltip
                let td_title = data.node.tr.querySelector('td.title_col')
                let timeout = null;

                // Function that will fetch the tooltip content and set it if not empty
                function prepare_tooltip(instance) {
                    // console.log("ajax");
                    fetch("/api/html/tooltip/specification/" + node.refKey)
                        .then((response) => response.text())
                        .then((html) => {
                            // console.log("done")
                            if (html) {
                                instance.setContent(html);
                                node.data['tooltip'] = html
                            } else {
                                instance.disable();
                                node.data['tooltip'] = null;
                            }
                        }).finally(() => {
                        timeout = null;
                    })
                }

                // Init empty tooltip
                tippy(td_title, {
                    content: '',
                    allowHTML: true,
                    followCursor: 'cursor',
                    interactive: true,
                    placement: 'bottom',
                    offset: [10, 20],
                    maxWidth: 'none',
                    delay: [650, 0],
                    duration: [150, 60],
                    onTrigger(instance, event) {
                        // Wait timeout, if long enough, call xhr request to init tooltip content
                        if (node.data['tooltip'] === undefined) {
                            // console.log("trigger timeout")
                            timeout = setTimeout(prepare_tooltip, 500, instance)
                        }
                    },
                    onUntrigger(instance, event) {
                        if (node.data['tooltip'] === undefined) {
                            clearTimeout(timeout)
                            timeout = null;
                            // console.log("clear timeout")
                        }
                    },
                    onCreate(instance) {
                        // Try to reuse data from previous async fetch tooltip
                        // Not sure that it's useful
                        if (node.data['tooltip']) {
                            instance.setContent(node.data['tooltip'])
                            // console.log("reuse")
                        }
                    }
                })
            }
            // Continue to render column
            $tdList.eq(2).html(() => {
                // Display tags
                let val = '';
                node.data['tags'].forEach((tag_id)=>{
                    val += render_tag(tag_id);
                })
                return val;
            })
            $tdList.eq(3).text(node.data['cost'].toFixed(0));
            $tdList.eq(4).html(() => {
                // Display supervisors column
                let val = '';
                if (node.data['supervisors']['directs'].length > 0) {
                    for (let i in node.data['supervisors']['directs']) {
                        val += "<span class='badge bg-primary direct-supervisors'>" + node.data['supervisors']['directs'][i] + "</span>";
                    }
                } else {
                    for (let i in node.data['supervisors']['parents']) {
                        let supervisor_class = 'badge';
                        if (node.data['supervisors']['conflicts'].includes(node.data['supervisors']['parents'][i])) {
                            supervisor_class += ' bg-warning text-black';
                        } else {
                            supervisor_class += ' bg-secondary'
                        }
                        val += "<span class='" + supervisor_class + "'>" + node.data['supervisors']['parents'][i] + "</span>";
                    }
                }
                return val;
            });
            $tdList.eq(5).html(() => {
                // Display concerned column
                let val = '';
                if (node.data['concerned']['directs'].length > 0) {
                    for (let i in node.data['concerned']['directs']) {
                        val += "<span class='badge bg-primary direct-concerned'>" + node.data['concerned']['directs'][i] + "</span>";
                    }
                } else {
                    for (let i in node.data['concerned']['parents']) {
                        let concerned_class = 'badge bg-secondary';
                        val += "<span class='" + concerned_class + "'>" + node.data['concerned']['parents'][i] + "</span>";
                    }
                }
                return val;
            });
            $tdList.eq(6).find('.progress').html(() => {
                // Display progress column
                let extra_style = '';
                if ((node.data["fulfill"] + node.data["childs_fulfill"]) > 100) {
                    extra_style = ' style="color: brown" ';
                }
                return '<div class="progress-bar" style="width: ' + node.data["fulfill"] + '%;" ></div>' +
                    '<div class="progress-bar bg-success" style="width: ' + node.data["childs_fulfill"] + '%;" ></div>' +
                    '<small class="justify-content-center d-flex position-absolute w-100" ' + extra_style + '>' + (node.data['fulfill'] + node.data["childs_fulfill"]).toFixed(0) + ' %</small>';
            });
        };

        fancytree_config.tooltip = false;

        fancytree_config.postProcess = function (event, data) {
            // Get the 'tree' object from AJAX source
            data.result = data.response.tree;
            let additional_contextmenu = {
                    "sep_tags": "----",
            }

            if (data.response.supervisors){
                pspec_supervisors = data.response.supervisors;

                let menu_supervisors_list = {};

                for (const [key, supervisor] of Object.entries(pspec_supervisors)) {
                    menu_supervisors_list[key] = {
                        name: pspec_supervisors[key]["username"],
                        isHtmlName: true,
                        callback: function (key, opt) {
                            let node = $.ui.fancytree.getNode(opt.$trigger);
                            toggle_supervisor(node.refKey, key)
                            // window.open("/admin/specification/specification/" + node.refKey + "/change/", '_blank').focus();
                        }
                    }
                }

                additional_contextmenu = {
                    ...additional_contextmenu,
                    "supervisors": {
                        name: _("Supervisors"),
                        icon: () => {
                            return "context-menu-icon bi-person-circle"
                        },
                        items: menu_supervisors_list
                    }
                }
            }

            if (data.response.tags) {
                pspec_tags = data.response.tags;

                let menu_tag_list = {};

                for (const [key, tag] of Object.entries(pspec_tags)) {
                    menu_tag_list[key] = {
                        name: render_tag(key),
                        isHtmlName: true,
                        callback: function (key, opt) {
                            let node = $.ui.fancytree.getNode(opt.$trigger);
                            toggle_tag(node.refKey, key)

                            // window.open("/admin/specification/specification/" + node.refKey + "/change/", '_blank').focus();
                        }
                    }
                }

                additional_contextmenu = {
                    ...additional_contextmenu,
                    "tags": {
                        name: _("Tags"),
                        icon: () => {
                            return "context-menu-icon bi-tags"
                        },
                        items: menu_tag_list
                    }
                }
            }

            if (data.response.actors){
                pspec_actors = data.response.actors;

                let menu_concerned_list = {};

                for (const [key, actor] of Object.entries(pspec_actors)) {
                    menu_concerned_list[key] = {
                        name: pspec_actors[key]["name"],
                        isHtmlName: true,
                        callback: function (key, opt) {
                            let node = $.ui.fancytree.getNode(opt.$trigger);
                            toggle_concerned(node.refKey, key)
                            // window.open("/admin/specification/specification/" + node.refKey + "/change/", '_blank').focus();
                        }
                    }
                }

                additional_contextmenu = {
                    ...additional_contextmenu,
                    "concerned": {
                        name: _("Concerned"),
                        icon: () => {
                            return "context-menu-icon bi-people-fill"
                        },
                        items: menu_concerned_list
                    }
                }
            }

            contextmenu_config.items = {
                "edit": {
                    name: _("Edit"),
                    icon: "edit",
                    callback: function (key, opt) {
                        let node = $.ui.fancytree.getNode(opt.$trigger);
                        start_edit_spec(node.refKey)

                        // window.open("/admin/specification/specification/" + node.refKey + "/change/", '_blank').focus();
                    }
                },
                "edit_parent": {
                    name: _("Edit relations"),
                    icon: () => {
                        return "context-menu-icon bi-bar-chart-steps"
                    },
                    callback: function (key, opt) {
                        let node = $.ui.fancytree.getNode(opt.$trigger);
                        start_edit_spec_relations(node.refKey)

                        // window.open("/admin/specification/specification/" + node.refKey + "/change/", '_blank').focus();
                    }
                },
                "add": {
                    name: _("Add spec included in this one"),
                    icon: "add",
                    callback: function (key, opt) {
                        let node = $.ui.fancytree.getNode(opt.$trigger);
                        start_create_spec(node.refKey);
                    }
                },
                "delete": {
                    name: _("Delete this specification (every where)"),
                    icon: "delete",
                    callback: function (key, opt) {
                        let node = $.ui.fancytree.getNode(opt.$trigger);
                        start_delete_spec(node.refKey);
                    }
                },
                "sep": "----",
                "edit_solutions": {
                    name: _("Manage solutions"),
                    icon: () => {
                        return "context-menu-icon bi-file-earmark-check"
                    },
                    callback: function (key, opt) {
                        let node = $.ui.fancytree.getNode(opt.$trigger);
                        start_edit_spec_solutions(node.refKey)
                    }
                },
                ...additional_contextmenu,
                "sep2": "----",
                ...contextmenu_config.items,
            }
            $.contextMenu(contextmenu_config);

        };

        fancytree_config.loadChildren = function (){
            // Re apply filter on load
            custom_tree_filter_update();
        }

        fancytree_config.treeId = "specs";
        fix_fancytree_persist_storage(fancytree_config.treeId);
        $("#tree")
            .fancytree(fancytree_config);

        $("input[name=search]").on("keyup", function (e) {
            // if(e.key === "Backspace" && e.target.value.length > 0){
            //     e.target.value = e.target.value.substring(0, e.target.value.length - 1);
            // }else if(e.key === " "){
            //     e.target.value += ' ';
            // }
            let tree = $.ui.fancytree.getTree();
            let match = $(this).val();

            if (match === "") {
                // tree.clearFilter();
                pspec_filter_title = null;
            } else {
                tree.filterNodes(match);
                pspec_filter_title = match;
            }
            custom_tree_filter_update();


        }).focus();

        $('#id_tags').on('change', function (){
            update_tags_selector();
            custom_tree_filter_update();});
        // Update tags selector on init
        update_tags_selector();

        $('#id_supervisors').on('change', function (){
            update_supervisors_selector();
            custom_tree_filter_update();});
        // Update supervisors selector on init
        update_supervisors_selector();

        $('#id_concerned').on('change', function (){
            update_concerned_selector();
            custom_tree_filter_update();});
        // Update concerned selector on init
        update_concerned_selector();
    }
)
;

function update_tags_selector() {
    // This function update the tags selector red number and the global var pspec_filter_tags
    let match = $('#id_tags').val();
    let indicator = $('#tags_selector_dropdown span');

    if (match.length > 0) {
        indicator.text(match.length);
        pspec_filter_tags = match
    } else {
        indicator.text("");
        pspec_filter_tags = null;
    }

}
function update_supervisors_selector() {
    // This function update the supervisors selector red number and the global var pspec_filter_supervisors
    let match = $('#id_supervisors').val();
    let indicator = $('#supervisors_selector_dropdown span');

    if (match.length > 0) {
        indicator.text(match.length);
        pspec_filter_supervisors = match
    } else {
        indicator.text("");
        pspec_filter_supervisors = null;
    }

}

function update_concerned_selector() {
    // This function update the concerned selector red number and the global var pspec_filter_concerned
    let match = $('#id_concerned').val();
    let indicator = $('#concerned_selector_dropdown span');

    if (match.length > 0) {
        indicator.text(match.length);
        pspec_filter_concerned = match
    } else {
        indicator.text("");
        pspec_filter_concerned = null;
    }

}

function _escapeRegex(str) {
		return (str + "").replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
	}
    function extractHtmlText(s) {
		if (s.indexOf(">") >= 0) {
			return $("<div/>").html(s).text();
		}
		return s;
	}

function get_title_filter(tree,
                          filter,
                          branchMode,
                          _opts
) {
    // This function is taken directly from jquery.fancytree.filter.js
    // It return a function that filter the title by a string and highlight the match
    var match,
        statusNode,
        re,
        reHighlight,
        reExoticStartChar,
        reExoticEndChar,
        temp,
        prevEnableUpdate,
        count = 0,
        treeOpts = tree.options,
        escapeTitles = treeOpts.escapeTitles,
        prevAutoCollapse = treeOpts.autoCollapse,
        opts = $.extend({}, treeOpts.filter, _opts),
        hideMode = opts.mode === "hide",
        leavesOnly = !!opts.leavesOnly && !branchMode;
    if (opts.fuzzy) {
        // See https://codereview.stackexchange.com/questions/23899/faster-javascript-fuzzy-string-matching-function/23905#23905
        // and http://www.quora.com/How-is-the-fuzzy-search-algorithm-in-Sublime-Text-designed
        // and http://www.dustindiaz.com/autocomplete-fuzzy-matching
        match = filter
            .split("")
            // Escaping the `filter` will not work because,
            // it gets further split into individual characters. So,
            // escape each character after splitting
            .map(_escapeRegex)
            .reduce(function (a, b) {
                // create capture groups for parts that comes before
                // the character
                return a + "([^" + b + "]*)" + b;
            }, "");
    } else {
        match = _escapeRegex(filter); // make sure a '.' is treated literally
    }
    re = new RegExp(match, "i");
    reHighlight = new RegExp(_escapeRegex(filter), "gi");
    if (escapeTitles) {
        reExoticStartChar = new RegExp(
            _escapeRegex(exoticStartChar),
            "g"
        );
        reExoticEndChar = new RegExp(_escapeRegex(exoticEndChar), "g");
    }
    filter = function (node) {
        if (!node.title) {
            return false;
        }
        var text = escapeTitles
                ? node.title
                : extractHtmlText(node.title),
            // `.match` instead of `.test` to get the capture groups
            res = text.match(re);
        if (res && opts.highlight) {
            if (escapeTitles) {
                if (opts.fuzzy) {
                    temp = _markFuzzyMatchedChars(
                        text,
                        res,
                        escapeTitles
                    );
                } else {
                    // #740: we must not apply the marks to escaped entity names, e.g. `&quot;`
                    // Use some exotic characters to mark matches:
                    temp = text.replace(reHighlight, function (s) {
                        return exoticStartChar + s + exoticEndChar;
                    });
                }
                // now we can escape the title...
                node.titleWithHighlight = escapeHtml(temp)
                    // ... and finally insert the desired `<mark>` tags
                    .replace(reExoticStartChar, "<mark>")
                    .replace(reExoticEndChar, "</mark>");
            } else {
                if (opts.fuzzy) {
                    node.titleWithHighlight = _markFuzzyMatchedChars(
                        text,
                        res
                    );
                } else {
                    node.titleWithHighlight = text.replace(
                        reHighlight,
                        function (s) {
                            return "<mark>" + s + "</mark>";
                        }
                    );
                }
            }
            // node.debug("filter", escapeTitles, text, node.titleWithHighlight);
        }
        return !!res;
    };
    return filter;
}

function custom_tree_filter_update() {
    // This function use global vars (pspec_filter_....) to filter the tree in order to allow
    //   multiple filter to be applied (title, tags, ...)

    let tree = $.ui.fancytree.getTree();
    let title_filter = (node) => {
        return true;
    };

    if (pspec_filter_concerned === null && pspec_filter_title === null && pspec_filter_supervisors === null && pspec_filter_tags === null) {
        tree.clearFilter();
        return;
    }
    if (pspec_filter_title !== null) {
        title_filter = get_title_filter(tree, pspec_filter_title)
    }

    tree.filterNodes(function (node) {
        if (pspec_filter_tags === null || (node.data['tags'] && node.data['tags'].some(elem => {
            return pspec_filter_tags.indexOf(elem.toString()) >= 0
        }))) {
            if (pspec_filter_concerned === null || pspec_filter_concerned.some(elem => {
                    let name = pspec_actors[elem]['name'];
                    return (node.data['concerned']['parents'].indexOf(name) >= 0) || (node.data['concerned']['directs'].indexOf(name) >= 0)
                })) {
                if (pspec_filter_supervisors === null || pspec_filter_supervisors.some(elem => {
                    let username = pspec_supervisors[elem]['username'];
                    return (node.data['supervisors']['parents'].indexOf(username) >= 0) || (node.data['supervisors']['directs'].indexOf(username) >= 0)
                })) {
                    if (pspec_filter_title === null || title_filter(node)) {
                        return true
                    }
                }
            }
        }
        return false;
    });

    // Start to collapse parents that aren't needed from old matches
    tree.visit((node) => {
        if (!node.match) {
            node.visitParents((parent) => {
                if (parent.isExpanded() && parent._filterAutoExpandedFixed) {
                    parent.setExpanded(false);
                    delete parent._filterAutoExpandedFixed;
                }
            });
        }
    });
    // Expand all parent from all matches
    tree.visit((node) => {
        if (node.match) {
            node.visitParents((parent) => {
                if (!parent.isExpanded()) {
                    parent.setExpanded(true);
                    parent._filterAutoExpandedFixed = true;
                }
            });
        }
    });

}
//
// function tag_filter(tags){
//     let tree = $.ui.fancytree.getTree();
//
//     tree.filterNodes(function(node) {
//   if( node.data['tags'] && node.data['tags'].some(elem => {return tags.indexOf(elem.toString()) >= 0})) {
//       console.log(node.data['tags'])
//     return true;
//   }else{
//       return false;
//   }
// });
//
// }