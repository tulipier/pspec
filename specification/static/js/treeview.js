function fix_fancytree_persist_storage(treeId="1"){
    let fancy_prefix = "fancytree-"+String(treeId);
    let storage = localStorage.getItem(fancy_prefix+'-expanded');
    if (storage){
        localStorage.setItem(fancy_prefix+'-expanded', storage.split('~').sort().join("~"))
    }
}

function toggle_select_all_tree(checkbox) {
    if (checkbox.checked !== true) {
        checkbox.checked = true
        checkbox.classList.add("fancytree-selected");
    } else {
        checkbox.checked = false;
        checkbox.classList.remove("fancytree-selected");
    }
    $.ui.fancytree.getTree("#tree").visit(function (node) {
        node.setSelected(checkbox.checked);
    });
}

function move_line_from_to(line_id, from_id, to_id, url) {
    console.log("Move " + line_id + " from " + from_id + " to " + to_id);
    $.ajax({
        type: "POST",
        url: url + line_id,
        data: {
            from: from_id,
            to: to_id,
            csrfmiddlewaretoken: window.csrftoken,
        },
    }).done(function (response) {
        console.log("Move success !");
        // $.ui.fancytree.getTree("#tree").reload(response);
        reload_tree_keep_scroll_position($.ui.fancytree.getTree("#tree"), response);
    }).fail(function (response) {
        console.log("Move fail !");
        console.log(this);
        console.log(response);
    });
}

function reload_tree_keep_scroll_position(tree, source){
    let scrollPos;
    let ret;
    if (typeof window.pageYOffset != 'undefined') {
        scrollPos = window.pageYOffset;
    }
    if (typeof document.compatMode != 'undefined' && document.compatMode != 'BackCompat') {
        scrollPos = document.documentElement.scrollTop;
    }
    else if (typeof document.body != 'undefined') {
        scrollPos = document.body.scrollTop;
    }
    // Deactivate node to avoid fancytree to try to scroll to the wrong position
    let focusNode = tree.getActiveNode()
    if (focusNode){
        focusNode.setFocus(false);
        focusNode.setActive(false);
    }
    fix_fancytree_persist_storage(tree.options.treeId);
    ret = tree.reload(source);
    ret.then(() => {
         window.scrollTo({
                top: scrollPos,
                left: undefined,
                behavior: 'instant',
            });
    })
    return ret;
}

let move_url = null;

let fancytree_config = {
    extensions: [
        // Table extension, see: https://github.com/mar10/fancytree/wiki/ExtTable
        "table",
        // Drag and drop extension, see: https://github.com/mar10/fancytree/wiki/ExtDnd5
        "dnd5",
        // Persist collapse state of item, see: https://github.com/mar10/fancytree/wiki/ExtPersist
        "persist",
        // Manage multiple node ref to the same spec, see: https://github.com/mar10/fancytree/wiki/ExtClones
        "clones",
        // Display child counter on icon, see: https://wwwendt.de/tech/fancytree/demo/index.html#sample-ext-childcounter.html
        "childcounter",
        // Add filtering and search, see: https://github.com/mar10/fancytree/wiki/ExtFilter
        "filter",
    ],
    // Load data from server
    source: {
        url: null,  // "/api/specification/tree",
        cache: false
    },
    // To uniquely identify tree
    treeId: 1,
    // Translations
    strings: {
        loading: _("Loading..."),
        loadError: _("Load error!"),
        moreData: _("More..."),
        noData: _("No data.")
    },
    // Set debug level of fancytree, 0:quiet, 1:info, 2:debug
    debugLevel: 1,
    // Manage tooltip
    tooltip: false,
    // -- Check box --
    // Add checkbox for each line
    checkbox: true,
    // Hierarchical selection
    selectMode: 3,
    // -- Keyboard --
    // Disallow titles to be reach by TAB (needed for search)
    titlesTabbable: false,
    // -- Toggle --
    // No effect on toggle line
    toggleEffect: false,
    // Enable icon (needed by child counter)
    icon: true,
    // Don't jump to nodes when pressing first character
    quicksearch: false,
    // Remove keyboard navigation to easily use search input
    keyboard: false,
    // -- CHILD COUNTER --
    childcounter: {
        deep: true,         // Count all childs
        hideZeros: true,    // Hide counter if no child
        hideExpanded: true  // Hide counter if node is expanded
    },
    // -- CLONE MODE --
    clones: {
        highlightClones: true
    },
    // -- FILTER AND SEARCH --
    filter: {
        counter: false,       // Don't add counter badge
        mode: 'hide',        // Hide non-matched nodes
        autoExpand: false,    // REPLACED BY CUSTOM LOGIC, do not set to true
        hideExpandedCounter: false,
    },
    // -- TABLE MODE --
    table: {
        // Display checkbox on first column
        checkboxColumnIdx: 0,
        // Display title on second column
        nodeColumnIdx: 1,
        // Indentation per level (in px)
        indentation: 24,
    },
    // -- PERSIST --
    persist: {
        // Use only local storage
        store: 'auto',
	types: 'expanded',
    },
    // -- DRAG AND DROP --
    dnd5: {
        autoExpandMS: 500,           // Expand nodes after n milliseconds of hovering.

        // --- Drag Support --------------------------------------------------------

        dragStart: function (node, data) {
            // Called on source node when user starts dragging `node`.
            // This method MUST be defined to enable dragging for tree nodes!
            // We can
            //   - Add or modify the drag data using `data.dataTransfer.setData()`.
            //   - Call `data.dataTransfer.setDragImage()` and set `data.useDefaultImage` to false.
            //   - Return false to cancel dragging of `node`.

            // Set the allowed effects (i.e. override the 'effectAllowed' option)
            data.effectAllowed = "all";  // or 'copyMove', 'link'', ...

            // Set a drop effect (i.e. override the 'dropEffectDefault' option)
            // One of 'copy', 'move', 'link'.
            // In order to use a common modifier key mapping, we can use the suggested value:
            data.dropEffect = data.dropEffectSuggested;

            // We could also define a custom image here (not on IE though):
            //data.dataTransfer.setDragImage($("<div>TEST</div>").appendTo("body")[0], -10, -10);
            //data.useDefaultImage = false;

            // Return true to allow the drag operation
            if (node.isFolder()) {
                return false;
            }
            return true;
        },
        dragDrag: function (node, data) {
            // Called on source node every few milliseconds while `node` is dragged.
            // Implementation of this callback is optional and rarely required.
        },
        dragEnd: function (node, data) {
            // Called on source node when the drag operation has terminated.
            // Check `data.isCancelled` to see if a drop occurred.
            // Implementation of this callback is optional and rarely required.
            // Note caveat:
            // If the drop handler removed or moved the dragged source element,
            // `node` and `data` may not contain expected values, or this event
            // is not triggered at all.
        },

        // --- Drop Support --------------------------------------------------------

        dragEnter: function (node, data) {
            // Called on target node when s.th. is dragged over `node`.
            // `data.otherNode` may be a Fancytree source node or null for
            // non-Fancytree droppables.
            // This method MUST be defined to enable dropping over tree nodes!
            //
            // We may
            //   - Set `data.dropEffect` (defaults to '')
            //   - Call `data.setDragImage()`
            //
            // Return
            //   - true to allow dropping (calc the hitMode from the cursor position)
            //   - false to prevent dropping (dragOver and dragLeave are not called)
            //   - a list (e.g. ["before", "after"]) to restrict available hitModes
            //   - a string "over", "before, or "after" to force a hitMode
            //   - Any other return value will calc the hitMode from the cursor position.

            // Example:
            // Prevent dropping a parent below another parent (only sort nodes under
            // the same parent):
            //if(node.parent !== data.otherNode.parent){
            //  return false;
            //}
            // Example:
            // Don't allow dropping *over* a node (which would create a child). Just
            // allow changing the order:
            //return ["before", "after"];

            // Accept everything:
            return true;
        },
        dragOver: function (node, data) {
            // Called on target node every few milliseconds while some source is
            // dragged over it.
            // `data.hitMode` contains the calculated insertion point, based on cursor
            // position and the response of `dragEnter`.
            //
            // We may
            //   - Override `data.hitMode`
            //   - Set `data.dropEffect` (defaults to the value that of dragEnter)
            //     (Note: IE will ignore this and use the value from dragenter instead!)
            //   - Call `data.dataTransfer.setDragImage()`

            // Set a drop effect (i.e. override the 'dropEffectDefault' option)
            // One of 'copy', 'move', 'link'.
            // In order to use a common modifier key mapping, we can use the suggested value:
            data.dropEffect = data.dropEffectSuggested;
        },
        dragExpand: function (node, data) {
            // Called when a dragging cursor lingers over a parent node.
            // (Optional) Return false to prevent auto-expanding `node`.
        },
        dragLeave: function (node, data) {
            // Called when s.th. is no longer dragged over `node`.
            // Implementation of this callback is optional and rarely required.
        },
        dragDrop: function (node, data) {
            // This function MUST be defined to enable dropping of items on the tree.
            //
            // The source data is provided in several formats:
            //   `data.otherNode` (null if it's not a FancytreeNode from the same page)
            //   `data.otherNodeData` (Json object; null if it's not a FancytreeNode)
            //   `data.dataTransfer.getData()`
            //
            // We may access some metadata to decide what to do:
            //   `data.hitMode` ("before", "after", or "over").
            //   `data.dataTransfer.dropEffect`,`.effectAllowed`
            //   `data.originalEvent.shiftKey`, ...
            //

            // spec dragged
            let line_id = data.otherNodeData.refKey;
            // from (previous parent)
            let from_id = data.otherNode.parent.refKey;
            // to (future parent), if not over, it's the parent of the target node
            let to_id = node.parent.refKey;
            // if it's over the node, then it will be the new parent
            if (data.hitMode === "over") {
                to_id = node.refKey;
            }

            move_line_from_to(line_id, from_id, to_id, move_url);
        }
    },
    // // -- CONTEXT MENU --
    // contextMenu: {
    //     menu: {
    //         "edit": { "name": "Edit", "icon": "edit" },
    //     }
    //  },
};

let contextmenu_config = {
    selector: "#tree span.fancytree-node",
    items: {
        "open_all": {
            name: _("Open all sub-specs"),
            icon: () => {
                return "context-menu-icon bi-arrows-expand"
            },
            callback: function (key, opt) {
                let node = $.ui.fancytree.getNode(opt.$trigger);
                node.visit(function (child) {
                    child.setExpanded(true);
                }, true);
            }
        },
        "close_all": {
            name: _("Close all sub-specs"),
            icon: () => {
                return "context-menu-icon bi-arrows-collapse"
            },
            callback: function (key, opt) {
                let node = $.ui.fancytree.getNode(opt.$trigger);
                node.visit(function (child) {
                    child.setExpanded(false);
                }, true);
            }
        },
        // "cut": {name: "Cut", icon: "cut",
        //     callback: function(key, opt){
        //       var node = $.ui.fancytree.getNode(opt.$trigger);
        //       alert("Clicked on " + key + " on " + node);
        //     }
        //   },
        // "copy": {name: "Copy", icon: "copy"},
        // "paste": {name: "Paste", icon: "paste", disabled: false },
        // "sep1": "----",
        // "edit": {name: "Edit", icon: "edit", disabled: true },
        // "delete": {name: "Delete", icon: "delete", disabled: true },
        // "more": {name: "More", items: {
        //   "sub1": {name: "Sub 1"},
        //   "sub1": {name: "Sub 2"}
        //   }}
    },
    // callback: function (itemKey, opt) {
    //     var node = $.ui.fancytree.getNode(opt.$trigger);
    //     alert("select " + itemKey + " on " + node);
    // }
};
