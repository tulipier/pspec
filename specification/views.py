import re

from django.shortcuts import render, redirect
from django.db import transaction, Error
from django.http.response import HttpResponseBadRequest, HttpResponseRedirect, HttpResponse
from django.http.request import HttpRequest
from django.utils import translation
from django.conf import settings
from django.utils.translation import gettext_lazy as _

from django.views.generic.base import View, TemplateView, ContextMixin
from django.views.generic.edit import DeleteView, UpdateView, CreateView, ModelFormMixin, FormView
import django.contrib.auth.views
from django.contrib.auth.mixins import LoginRequiredMixin
from django.forms.widgets import HiddenInput, Textarea
from django.forms import inlineformset_factory

from specification.forms import VisualizeSpecsAdminForm, VisualizeSpecsForm, LoginForm, PspecHTMLCRUDMixin, \
    PspecHTMLFormMixin, TagsSelectorForm, SupervisorsSelectorForm, ConcernedSelectorForm
from specification.models import Specification, Project
import specification.specviz

import solution.models
import solution.forms

from multiselect_dropdown.widgets import MultiSelectDropdown
from slimselect.widgets import MultiSlimSelect

import plantweb.render


class SiteViewMixin(ContextMixin):
    page = ""

    def get_context_data(self, **kwargs: 'Any') -> 'Dict[str, Any]':
        context = super(SiteViewMixin, self).get_context_data(**kwargs)
        context['page'] = self.page
        context['user_projects'] = Project.objects.filter(users=self.request.user)
        context['user_solutions'] = list()
        if self.request.session.get('show_solutions', False):
            context['user_solutions'] = solution.models.SolutionSet.objects.filter(
                project_id=self.request.session['current_project_id'])
        return context


class TestView(TemplateView):
    template_name = "specification/test.html"


class TestScratchView(TemplateView):
    template_name = "specification/test_scratch.html"


class TreeTableView(LoginRequiredMixin, SiteViewMixin, TemplateView):
    page = "treetable"
    template_name = "specification/treetable.html"

    def get_context_data(self, **kwargs):
        context = super(TreeTableView, self).get_context_data(**kwargs)
        context['tags_form'] = TagsSelectorForm(self.request.session['current_project_id'])
        context['supervisors_form'] = SupervisorsSelectorForm(self.request.session['current_project_id'])
        context['concerned_form'] = ConcernedSelectorForm(self.request.session['current_project_id'])
        return context


class LoginView(django.contrib.auth.views.LoginView):
    template_name = "specification/login.html"
    authentication_form = LoginForm
    next_page = "/treetable"

    def get(self, request: HttpRequest, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect("/treetable")
        return super(LoginView, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        """Security check complete. Log the user in."""
        response = super(LoginView, self).form_valid(form)
        if self.request.user.preferences is not None:
            if self.request.user.preferences.default_project is not None:
                self.request.user.preferences.default_project.set_active_for_session(self.request.session)
            # Set language for the user
            translation.activate(self.request.user.preferences.language)
            response.set_cookie(settings.LANGUAGE_COOKIE_NAME, self.request.user.preferences.language)
            self.request.session['show_solutions'] = self.request.user.preferences.show_solutions
        return response


class LogoutView(django.contrib.auth.views.LogoutView):
    template_name = "specification/login.html"
    next_page = "/"


class ChangePasswordView(django.contrib.auth.views.PasswordChangeView):
    def get_success_url(self, *args, **kwargs):
        return "/"


class ProjectActivateView(View):
    def get(self, request: HttpRequest, project_id):
        try:
            project = Project.objects.get(pk=project_id)
            project.set_active_for_session(request.session)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
        except Project.DoesNotExist:
            return HttpResponseBadRequest()


class SpecificationEditView(PspecHTMLCRUDMixin, UpdateView):
    model = Specification
    fields = [
        'title',
        'description',
        'supervisors',
        'concerned',
        'cost',
    ]
    widgets = {
        'description': Textarea(attrs={
            'rows': 5
        }),
        'supervisors': MultiSlimSelect(attrs={
            'placeholder': _("Select one or many supervisor(s)")
        }),
        'concerned': MultiSlimSelect(attrs={
            'placeholder': _("Select one or many concerned actor(s)")
        }),
    }
    querysets = {
        'supervisors': PspecHTMLFormMixin.filter_project_users,
        'concerned': PspecHTMLFormMixin.filter_project_actors,
    }

    def get_form(self):
        form = super(SpecificationEditView, self).get_form()
        if self.object and self.object.included_in.count() > 0:
            # Disable cost field if there is a parent to this spec
            form.fields['cost'].disabled = True
        return form

    def get_post_url(self):
        return f"/api/html/edit/{self.model_name}/{self.object.pk}"


class SpecificationIncludedInByMixin:
    def get_context_data(self, **kwargs):
        context = super(SpecificationIncludedInByMixin, self).get_context_data(**kwargs)
        included_in_formset = inlineformset_factory(
            Specification,
            specification.models.SpecificationIncludedIn,
            form=specification.forms.SpecificationIncludedInForm,
            fk_name='from_specification',
            extra=1,
            can_delete=True
        )
        included_in_formset.form.base_fields['to_specification'].queryset = self.filter_project_specification()
        if self.request.POST:
            context['included_in_formset'] = included_in_formset(self.request.POST, instance=self.object)
        else:
            context['included_in_formset'] = included_in_formset(instance=self.object)
        included_by_formset = inlineformset_factory(
            Specification,
            specification.models.SpecificationIncludedIn,
            form=specification.forms.SpecificationIncludedByForm,
            fk_name='to_specification',
            extra=1,
            can_delete=True
        )
        included_by_formset.form.base_fields['from_specification'].queryset = self.filter_project_specification()
        if self.request.POST:
            context['included_by_formset'] = included_by_formset(self.request.POST, instance=self.object)
        else:
            context['included_by_formset'] = included_by_formset(instance=self.object)
        return context

    def form_valid(self, form):
        context = self.get_context_data()
        included_in_formset = context['included_in_formset']
        included_by_formset = context['included_by_formset']
        try:
            with transaction.atomic():
                ret = super(SpecificationIncludedInByMixin, self).form_valid(form)
                if included_in_formset.is_valid():
                    included_in_formset.instance = self.object
                    included_in_formset.save()
                    if included_by_formset.is_valid():
                        included_by_formset.instance = self.object
                        included_by_formset.save()
                        return ret
                else:
                    raise Error
        except Error:
            return HttpResponseBadRequest()


class SpecificationEditRelationsView(SpecificationIncludedInByMixin, PspecHTMLCRUDMixin, UpdateView):
    model = Specification
    template_name_suffix = "_with_relations"
    fields = []

    def get_post_url(self):
        return f"/api/html/edit_relations/{self.model_name}/{self.object.pk}"


class SpecificationDeleteView(PspecHTMLCRUDMixin, DeleteView):
    model = Specification

    def get_post_url(self):
        return f"/api/html/delete/{self.model_name}/{self.object.pk}"


class SpecificationCreateView(SpecificationIncludedInByMixin, PspecHTMLCRUDMixin, CreateView):
    model = Specification
    template_name_suffix = "_with_relations"
    post_url = "/api/html/create/specification/"
    fields = [
        'project',
        'title',
        'description',
        'supervisors',
        'concerned',
        'cost'
    ]
    widgets = {
        'project': HiddenInput(),
        'description': Textarea(attrs={
            'rows': 5
        }),
        'supervisors': MultiSlimSelect(attrs={
            'placeholder': _("Select one or many supervisor(s)")
        }),
        'concerned': MultiSlimSelect(attrs={
            'placeholder': _("Select one or many concerned actor(s)")
        }),
    }
    querysets = {
        'supervisors': PspecHTMLFormMixin.filter_project_users,
        'concerned': PspecHTMLFormMixin.filter_project_actors,
    }

    def get_initial(self):
        initial = super(SpecificationCreateView, self).get_initial()
        initial['project'] = self.request.session['current_project_id']
        return initial


class ManageSolutionsForSpecificationView(solution.forms.ManageSolutionElementMixin, UpdateView):
    model = Specification
    post_url = '/api/html/manage_solutions/specification/'
    fields = [

    ]


class SpecificationTooltipView(TemplateView):
    template_name = "specification/htmx/tooltip.html"

    def get_context_data(self, **kwargs):
        context = super(SpecificationTooltipView, self).get_context_data(**kwargs)
        context['spec'] = Specification.objects.get(pk=kwargs['pk'])
        return context


class SpecificationVisualizeView(PspecHTMLFormMixin, FormView):
    template_name = "specification/specification_form.html"
    form_class = VisualizeSpecsForm
    post_url = "/api/html/visualize/specification/"
    querysets = {
        'specs': PspecHTMLFormMixin.filter_project_specification
    }

    def form_valid(self, form: VisualizeSpecsForm):
        visu_class = None
        if form.cleaned_data['visualize_mode'] == "mindmap":
            visu_class = specification.specviz.PlantUMLVizMindmap
        elif form.cleaned_data['visualize_mode'] == "deploy":
            visu_class = specification.specviz.PlantUMLVizDeploy
        elif form.cleaned_data['visualize_mode'] == "deploy_nested":
            visu_class = specification.specviz.PlantUMLVizDeployNested
        elif form.cleaned_data['visualize_mode'] == "wbs":
            visu_class = specification.specviz.PlantUMLVizWBS

        specs = form.cleaned_data['specs']
        visu = visu_class(specs)
        if form.cleaned_data['tags']:
            visu.activate_tag_render(re.compile(form.cleaned_data['tags_regex']))
        plantuml = visu.generate_plantuml_content()
        graph = plantweb.render.render(plantuml, engine="plantuml")
        graph = graph[0].decode("utf8")
        return render(self.request, "specification/visualize.html", context={
            "graph": graph,
            "plantuml": plantuml
        })


class VisualizeSpecificationView(SpecificationVisualizeView):
    def post(self, request, *args, **kwargs):
        form = VisualizeSpecsAdminForm(Specification.objects.filter(project__pk=request.session["current_project_id"]),
                                       None, request.POST)
        if form.is_valid():
            return self.form_valid(form)
        else:
            print(f"{form.errors}")


class ChangePasswordApiView(PspecHTMLFormMixin, django.contrib.auth.views.PasswordChangeView):
    template_name = "specification/specification_form.html"
    post_url = "/api/html/accounts/password_change/"
    success_url = "/"

    def form_invalid(self, form):
        ret = super(ChangePasswordApiView, self).form_invalid(form)
        ret.status_code = 400
        return ret

    def form_valid(self, form: VisualizeSpecsAdminForm):
        ret = super(ChangePasswordApiView, self).form_valid(form)
        return HttpResponse()
