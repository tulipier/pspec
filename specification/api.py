from django.core.serializers.json import DjangoJSONEncoder
from django.http.response import JsonResponse
from django.db import transaction, Error
from django.http.response import HttpResponseBadRequest, HttpResponse

from specification.models import Specification, Project, Tag, Actor
from django.contrib.auth.models import User

try:
    from solution.models import SolutionElement
except ImportError:
    SolutionElement = None

from rest_framework.views import APIView

from specification.tree import SpecificationTree


class SetEncoder(DjangoJSONEncoder):
    """
    Allow to export set type to JSON
    """
    def default(self, obj):
        if isinstance(obj, set):
            return list(obj)
        return DjangoJSONEncoder.default(self, obj)


class SpecificationApi(APIView):
    def get(self, request, format=None):
        project = Project.objects.get(pk=request.session["current_project_id"])
        specs = Specification.objects.filter(project__pk=project.pk)
        parser = SpecificationTree(specs, request.session["current_project_id"])
        parser.add_cost_to_graph()
        tree = parser.get_nested_tree()
        total_fulfillment = parser.add_fulfillment_to_tree(tree, request.session['current_solution_id'])
        parser.add_supervisors_to_tree(tree)
        parser.add_concerned_to_tree(tree)
        # tree[0]["title"] = f"—— {project.name} ——"
        return JsonResponse({
            'tree': tree,
            'tags': dict((o['pk'], o) for o in Tag.objects.filter(
                project_id=request.session["current_project_id"]).values(
                'pk', 'name', 'color', 'text_color')),
            'supervisors': dict((o['pk'], o) for o in User.objects.filter(
                project=request.session["current_project_id"]).values(
                'pk', 'username')),
            'actors': dict((o['pk'], o) for o in Actor.objects.filter(
                project=request.session["current_project_id"]).values(
                'pk', 'name'))
        }, safe=False, encoder=SetEncoder)


class SpecificationMoveApi(APIView):
    def post(self, request, spec_id):
        try:
            with transaction.atomic():
                project = Project.objects.get(pk=request.session["current_project_id"])
                spec_node = Specification.objects.get(pk=spec_id, project__pk=project.pk)
                if request.POST.get('from', '') != '':
                    spec_from = Specification.objects.get(pk=request.POST.get('from'), project__pk=project.pk)
                    spec_node.included_in.remove(spec_from)
                if request.POST.get('to', '') != '':
                    spec_to = Specification.objects.get(pk=request.POST.get('to'), project__pk=project.pk)
                    spec_node.included_in.add(spec_to)
            return SpecificationApi().get(request)
        except Error:
            return HttpResponseBadRequest()


class ProjectActivateApi(APIView):
    def get(self, request, project_id):
        try:
            project = Project.objects.get(pk=project_id)
            project.set_active_for_session(request.session)
            return HttpResponse()
        except Project.DoesNotExist:
            return HttpResponseBadRequest()


class ToggleSupervisorAPI(APIView):
    def get(self, request, spec_id, supervisor_id):
        try:
            spec = Specification.objects.get(pk=spec_id)
            supervisor = User.objects.get(pk=supervisor_id)
            if spec.supervisors.filter(id=supervisor_id).exists():
                spec.supervisors.remove(supervisor)
            else:
                spec.supervisors.add(supervisor)
            return HttpResponse()
        except (Specification.DoesNotExist, User.DoesNotExist):
            return HttpResponseBadRequest()


class ToggleTagAPI(APIView):
    def get(self, request, spec_id, tag_id):
        try:
            spec = Specification.objects.get(pk=spec_id)
            tag = Tag.objects.get(pk=tag_id)
            if spec.tag_set.filter(id=tag_id).exists():
                spec.tag_set.remove(tag)
            else:
                spec.tag_set.add(tag)
            return HttpResponse()
        except (Specification.DoesNotExist, Tag.DoesNotExist):
            return HttpResponseBadRequest()


class ToggleConcernedAPI(APIView):
    def get(self, request, spec_id, actor_id):
        try:
            spec = Specification.objects.get(pk=spec_id)
            actor = Actor.objects.get(pk=actor_id)
            if spec.concerned.filter(id=actor_id).exists():
                spec.concerned.remove(actor)
            else:
                spec.concerned.add(actor)
            return HttpResponse()
        except (Specification.DoesNotExist, Actor.DoesNotExist):
            return HttpResponseBadRequest()
