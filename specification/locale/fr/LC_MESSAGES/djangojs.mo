��          �      |      �     �            	   3     =  '   I     q     v  	   �     �  
   �     �     �     �     �     �     �     �     �            A    (   Z     �  (   �     �     �  (   �          "     8     R     i     w     �     �     �  &   �     �     �     �       	   4     
                                          	                                                  Add spec included in this one Change Password Close all sub-specs Concerned Delete spec Delete this specification (every where) Edit Edit relations Edit spec Load error! Loading... Manage solutions More... New spec No data. Open all sub-specs Supervisors Tags Visualize specs add another remove Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Ajouter une spécification dans celle-ci Changer de mot de passe Refermer toutes les sous-spécifications Concerné⋅e(s) Supprimer le spécification Supprimer cette spécification (partout) Éditer Éditer les relations Éditer la spécification Erreur de chargement ! Chargement... Gérer les solutions Plus... Nouvelle spécification Pas de donnée. Ouvrir toutes les sous-spécifications Référent⋅e(s) Tags Visualiser les spécifications ajouter un autre élément supprimer 