from abc import abstractmethod
import re

from plantweb.render import render

from specification.models import Specification, Tag
from django.db.models.query import QuerySet

from html import escape as html_escape
import networkx


def escape(s, quote=False):
    return html_escape(s, quote)


class PlantUMLViz:
    DEFAULT_OPTIONS = {}

    def __init__(self, specs: 'QuerySet[Specification]', options=None, headers=None):
        self.specs = specs
        if len(specs) > 0 and hasattr(specs[0], 'project'):
            self.project = specs[0].project
        else:
            self.project = None
        self.options = dict(PlantUMLViz.DEFAULT_OPTIONS) if options is None else options
        self.graph = networkx.DiGraph()
        self.headers = headers if headers is not None else {}
        self.create_graph()
        self.show_tags = False
        self.show_tags_regex = None

    def activate_tag_render(self, regex: 're.Pattern'):
        self.show_tags = True
        self.show_tags_regex = regex
        if 'tag_procedure' not in self.headers.keys():
            self.headers['tag_procedure'] = "!function $tag($txt, $bcolor, $tcolor)\n"+\
                                            """!return "<B><back:"+$bcolor+"><color:"+$tcolor+"><U+00A0>"+$txt+"<U+00A0></color></back></B>"\n"""+\
                                            "!endfunction\n"

    def render_headers(self):
        return "\n".join(self.headers.values())

    def render_tag(self, tag: Tag):
        return f"""$tag("{escape(tag.name)}", "{tag.color}", "{tag.text_color}")"""

    def create_graph(self):
        for spec in self.specs:
            self.graph.add_node(spec.pk, data=self.SpecViz(spec, self))
        for spec in self.specs:
            for spec_inc in spec.included_in.all():
                if spec_inc.pk in self.graph.nodes():
                    self.graph.add_edge(spec_inc.pk, spec.pk)


    @abstractmethod
    def generate_plantuml_content(self):
        pass

    class SpecViz:
        def __init__(self, spec: 'Specification', viz: 'PlantUMLViz', *args, **kwargs):
            self.spec = spec
            self.viz = viz

        def render_tags(self):
            if self.viz.show_tags:
                content = " ".join([self.viz.render_tag(tag) for tag in self.spec.tag_set.all()
                                    if self.viz.show_tags_regex is None or self.viz.show_tags_regex.match(tag.name)])
                if len(content)>0:  # Add space if there is at least one tag
                    content += " "
                return content
            else:
                return ""

        @abstractmethod
        def render(self, viz, *args, **kwargs):
            pass


class PlantUMLVizMindmap(PlantUMLViz):

    def generate_plantuml_content(self):
        body = ""
        for source in [n for n in self.graph.nodes() if self.graph.in_degree(n) == 0]:
            body += self.graph.nodes[source]['data'].render(self, depth=1)
        content = "@startmindmap\n"+self.render_headers()+body+"@endmindmap"
        # for source in [n for n in self.graph.nodes() if self.graph.in_degree(n) == 0 and 'data' in self.graph.nodes[n].keys()]:
        return content

    class SpecViz(PlantUMLViz.SpecViz):

        def render(self, viz, depth, *args, **kwargs):
            content = "*" * depth
            content += f" {self.render_tags()}{escape(self.spec.title)}\n"
            for successor in [ node for node in viz.graph.successors(self.spec.pk) if 'data' in viz.graph.nodes[node].keys() ]:
                content += viz.graph.nodes[successor]['data'].render(viz, depth=depth+1)
            return content


class PlantUMLVizWBS(PlantUMLVizMindmap):

    def generate_plantuml_content(self):
        content = "@startwbs\n"
        content += self.render_headers()
        if self.project is not None:
            content += f"* {self.project.name}\n"
        for source in [n for n in self.graph.nodes() if self.graph.in_degree(n) == 0]:
            content += self.graph.nodes[source]['data'].render(self, depth=2)
        content += "@endwbs"
        return content


class PlantUMLVizDeploy(PlantUMLViz):
    plantuml_elem = 'card'
    plantuml_blocked_by = '<.u.'    # 'u' allow to keep logical and hierarchical order
    plantuml_included_in = '==>'

    def generate_plantuml_content(self):
        content = "@startuml\nskinparam linetype polyline\n"
        content += self.render_headers()
        for source in [n for n in self.graph.nodes() if self.graph.in_degree(n) == 0]:
            content += self.graph.nodes[source]['data'].render(self, depth=0)
        for node in [ node for node in self.graph.nodes() if 'data' in self.graph.nodes[node].keys() ]:
            content += self.graph.nodes[node]['data'].render_links(self)
        content += "@enduml"
        return content

    class SpecViz(PlantUMLViz.SpecViz):
        def render(self, viz, depth=0, *args, **kwargs):
            elem_id = f"id{self.spec.pk}"
            content = f"{PlantUMLVizDeploy.plantuml_elem} \"{self.render_tags()}{escape(self.spec.title)}\" as {elem_id}\n"
            for successor in [ node for node in viz.graph.successors(self.spec.pk) if 'data' in viz.graph.nodes[node].keys() ]:
                content += viz.graph.nodes[successor]['data'].render(viz, depth=depth + 1)
            return content

        def render_links(self, viz, *args, **kwargs):
            content = ""
            for spec_inc in [node for node in self.spec.included_in.all() if 'data' in viz.graph.nodes[node.pk].keys()]:
                    content += f"id{spec_inc.pk} {PlantUMLVizDeploy.plantuml_included_in} id{self.spec.pk}\n"
            for spec_bloc in [node for node in self.spec.blocked_by.all() if 'data' in viz.graph.nodes[node.pk].keys()]:
                    content += f"id{spec_bloc.pk} {PlantUMLVizDeploy.plantuml_blocked_by} id{self.spec.pk}\n"
            return content


class PlantUMLVizDeployNested(PlantUMLViz):

    def generate_plantuml_content(self):
        content = "@startuml\nskinparam linetype polyline\nleft to right direction\n"
        content += self.render_headers()
        for source in [n for n in self.graph.nodes() if self.graph.in_degree(n) == 0]:
            content += self.graph.nodes[source]['data'].render(self, depth=0)
        for node in [ node for node in self.graph.nodes() if 'data' in self.graph.nodes[node].keys() ]:
            content += self.graph.nodes[node]['data'].render_links(self)
        content += "@enduml"
        return content

    class SpecViz(PlantUMLViz.SpecViz):
        def __init__(self, *args, **kwargs):
            super(PlantUMLVizDeployNested.SpecViz, self).__init__(*args, **kwargs)
            self.all_ids = list()

        def get_new_id(self):
            elem_id = f"id{self.spec.pk}"
            if len(self.all_ids) > 0:
                last = self.all_ids[-1]
                if last == elem_id or last[-1] == 'z':
                    elem_id += 'a'
                else:
                    elem_id = elem_id[:-1] + chr(ord(last[-1]) + 1)
            self.all_ids.append(elem_id)
            return elem_id

        def render(self, viz, depth=0, *args, **kwargs):
            elem_id = self.get_new_id()
            content = ("  " * depth) + f"node \"{self.render_tags()}{escape(self.spec.title)}\" as {elem_id} {{\n"
            for successor in [ node for node in viz.graph.successors(self.spec.pk) if 'data' in viz.graph.nodes[node].keys() ]:
                content += viz.graph.nodes[successor]['data'].render(viz, depth=depth + 1)
            content += ("  " * depth) + "}\n"
            return content

        def render_links(self, viz, *args, **kwargs):
            content = ""
            for elem_id in self.all_ids:
                for spec_bloc in [node for node in self.spec.blocked_by.all() if 'data' in viz.graph.nodes[node.pk].keys()]:
                    for elem_bloc_id in viz.graph.nodes[spec_bloc.pk]['data'].all_ids:
                        content += f"{elem_bloc_id} <.. {elem_id}\n"
            return content


def test():
    # a = PlantUMLVizDeploy(Specification.objects.all())
    a = PlantUMLVizMindmap(Specification.objects.all())
    print(a.generate_plantuml_content())
