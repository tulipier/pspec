from copy import copy
from inspect import currentframe


def f_string_eval(s: str) -> str:
    """
    Format a string as an f-string without the need of the particular syntaxe
    Useful if the string need to be translated.
    Get from : https://stackoverflow.com/questions/49797658/how-to-use-gettext-with-python-3-6-f-strings 
    :param s: The string to format
    :return: The formatted string
    """
    frame = currentframe().f_back
    kwargs = copy(frame.f_globals)
    kwargs.update(frame.f_locals)
    return s.format(**kwargs)

