from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.forms.models import modelform_factory, ModelForm
from django.utils.translation import gettext_lazy as _

from django.db.models.query import QuerySet
from django.views.generic.edit import DeleteView, ModelFormMixin, FormView
from django.http.response import HttpResponse

from specification.models import Specification, SpecificationIncludedIn, Tag, Actor

from specification.tree import add_depth_to_graph
from bootstrap_hierarchy_select.widgets import BSHierarchySelect

from slimselect.widgets import MultiSlimSelect


class VisualizeSpecsForm(forms.Form):
    specs = forms.ModelMultipleChoiceField(
        queryset=Specification.objects.all(),
        required=True,
        widget=FilteredSelectMultiple('specs', False)
    )  # will be override in __init__
    visualize_mode = forms.ChoiceField(choices=(
        ('mindmap', _("PlantUML MindMap")),
        ('deploy', _("PlantUML Deploy")),
        ('deploy_nested', _("PlantUML Deploy (Nested)")),
        ('wbs', _("PlantUML WBS")),
    ))
    tags = forms.BooleanField(
        label=_("Show spec tags"),
        initial=True,
        required=False
    )
    tags_regex = forms.CharField(label=_("Filter tags to show [regex]"), required=False, initial=".*")


class VisualizeSpecsAdminForm(VisualizeSpecsForm):
    specs = forms.ModelMultipleChoiceField(queryset=None, required=False)  # will be override in __init__
    select_all = forms.BooleanField(initial=False, required=False)

    def __init__(self, specs: 'QuerySet[Specification]', select_all, *args, **kwargs):
        super(VisualizeSpecsAdminForm, self).__init__(*args, **kwargs)
        self.specs = specs
        self.fields['specs'] = forms.ModelMultipleChoiceField(queryset=specs, required=False)
        self.fields['select_all'].initial = False
        self.fields['select_all'].widget.attrs['hidden'] = True
        if select_all is True:
            self.fields['specs'].initial = self.specs.values_list('pk', flat=True)
            self.fields['select_all'].initial = 'on'
            self.fields['select_all'].widget.attrs['hidden'] = False
            # self.fields['select_all'].disabled = True
            self.fields['specs'].widget.attrs['hidden'] = False
            # self.fields['specs'].disabled = True
        else:
            if specs is not None:
                self.fields['specs'].initial = self.specs.values_list('pk', flat=True)


class LoginForm(AuthenticationForm):
    # login = forms.CharField()
    # password = forms.CharField(widget=forms.widgets.PasswordInput())
    pass


class PspecHTMLFormMixin(FormView):
    querysets = None    # Allow to override queryset
    widgets = None
    post_url = ""

    def filter_project_specification(self):
        """
        Allow to filter specification by project
        :return:
        """
        return Specification.objects.filter(project_id=self.request.session['current_project_id'])

    def filter_project_users(self):
        """
        Allow to filter users by project
        :return:
        """
        return User.objects.filter(project=self.request.session['current_project_id'])

    def filter_project_actors(self):
        """
        Allow to filter actors by project
        :return:
        """
        return Actor.objects.filter(project=self.request.session['current_project_id'])

    def get_post_url(self):
        return self.post_url

    def get_form(self, form_class=None):
        form = super(PspecHTMLFormMixin, self).get_form(form_class)
        # Override queryset
        if self.querysets is not None:
            for field_name, qs_callback in self.querysets.items():
                form.fields[field_name].queryset = qs_callback(self)
        return form


class PspecHTMLCRUDMixin(PspecHTMLFormMixin, ModelFormMixin):
    widgets = None
    fields = []

    def __init__(self, *args, **kwargs):
        super(PspecHTMLCRUDMixin, self).__init__(*args, **kwargs)
        self.model_name = self.model._meta.model_name

    def form_valid(self, form):
        """If the form is valid, save the associated model."""
        if not issubclass(self.__class__, DeleteView):
            self.object = form.save()
        else:
            self.object.delete()
        return HttpResponse()

    def get_form_class(self):
        """Get from : https://stackoverflow.com/questions/16937076/how-does-one-use-a-custom-widget-with-a-generic-updateview-without-having-to-red"""
        formclass = modelform_factory(self.model, fields=self.fields, widgets=self.widgets)
        return formclass


class SpecificationIncludedInForm(ModelForm):
    class Meta:
        model = SpecificationIncludedIn
        fields = ['to_specification', 'relative_cost']
        widgets = {
            'to_specification': BSHierarchySelect(depth_choices_fnct=add_depth_to_graph),
        }


class SpecificationIncludedByForm(ModelForm):
    class Meta:
        model = SpecificationIncludedIn
        fields = ['from_specification', 'relative_cost']
        widgets = {
            'from_specification': BSHierarchySelect(depth_choices_fnct=add_depth_to_graph),
        }


class TagsSelectorForm(forms.Form):
    tags = forms.ModelMultipleChoiceField(queryset=None, required=False)

    def __init__(self, project_id, *args, **kwargs):
        super(TagsSelectorForm, self).__init__(*args, **kwargs)
        self.fields['tags'] = forms.ModelMultipleChoiceField(
            queryset=Tag.objects.filter(project=project_id),
            widget=MultiSlimSelect(),
            label="",
            required=False)


class SupervisorsSelectorForm(forms.Form):
    supervisors = forms.ModelMultipleChoiceField(queryset=None, required=False)

    def __init__(self, project_id, *args, **kwargs):
        super(SupervisorsSelectorForm, self).__init__(*args, **kwargs)
        self.fields['supervisors'] = forms.ModelMultipleChoiceField(
            queryset=User.objects.filter(project=project_id),
            widget=MultiSlimSelect(),
            label="",
            required=False)


class ConcernedSelectorForm(forms.Form):
    concerned = forms.ModelMultipleChoiceField(queryset=None, required=False)

    def __init__(self, project_id, *args, **kwargs):
        super(ConcernedSelectorForm, self).__init__(*args, **kwargs)
        self.fields['concerned'] = forms.ModelMultipleChoiceField(
            queryset=Actor.objects.filter(project=project_id),
            widget=MultiSlimSelect(),
            label="",
            required=False)

