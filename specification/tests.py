#from django.test import TestCase

# Create your tests here.


import networkx as nx
import matplotlib.pyplot as plt


def _recursive_display_node(graph, node, depth):
    substructure = "*"*depth
    substructure += f" {node}\n"
    for subnode in [ subnode for subnode in graph.successors(node)]:
        substructure += _recursive_display_node(graph, subnode, depth+1)
    return substructure

def show_graph(graph):
    structure = ''
    for source in [n for n in graph.nodes() if graph.in_degree(n) == 0]:
        structure += _recursive_display_node(graph, source, 1)
    print(structure)


def get_graph_test1():
    graph = nx.DiGraph()
    graph.add_edge('A', 'B', weight=3)
    graph.add_edge('A', 'C', weight=2)
    graph.add_edge('A', 'D', weight=2)
    graph.add_edge('D', 'X', weight=1)
    graph.add_edge('D', 'Y', weight=1)
    graph.add_edge('E', 'F', weight=3)
    graph.add_edge('E', 'G', weight=2)
    graph.add_edge('G', 'C', weight=1)
    graph.add_edge('G', 'D', weight=3)
    graph.add_edge('D', 'C', weight=1)
    return graph


class Node:
    def __init__(self):
        intracost = 0
        computecost = 0

