from typing import Union
import networkx

from django.db.models.query import QuerySet
from django.db.models import Sum, Q, F
from django.db.models.functions import Coalesce
from django.db.models import Prefetch


import specification.models
from specification.models import Specification, SpecificationIncludedIn, Tag
import solution.models


class Tree:
    def __init__(self, nodes: 'QuerySet[Union[Specification, SolutionElement]]'):
        self.nodes = self.get_queryset(nodes)
        self.graph = networkx.DiGraph()
        for node in self.nodes:
            self.graph.add_node(node.pk, data=node)
        for node in self.nodes:
            for node_inc in node.included_in.all():
                if node_inc.pk in self.graph.nodes():
                    self.graph.add_edge(node_inc.pk, node.pk)

    def get_queryset(self,
                     nodes: 'QuerySet[Union[Specification, SolutionElement]]') -> 'QuerySet[Union[Specification, SolutionElement]]':
        return nodes.prefetch_related('included_in').all()


class ChoicesTree(Tree):
    _depth = None
    _choices = None

    def get_choices_with_depth(self):
        self._depth = list()
        self._choices = list()
        for source in [n for n in self.graph.nodes() if self.graph.in_degree(n) == 0]:
            self._parse_node(source, depth=1)
        return self._choices, self._depth

    def _add_node(self, node, depth):
        self._choices.append((self.graph.nodes[node]['data'].pk, str(self.graph.nodes[node]['data'])))
        self._depth.append(depth)

    def _parse_node(self, node, depth):
        self._add_node(node, depth)
        for successor in [next_node for next_node in self.graph.successors(node) if
                          'data' in self.graph.nodes[node].keys()]:
            self._parse_node(successor, depth + 1)


class FancyTree(Tree):
    project_id = None

    def __init__(self, nodes, project_id):
        super(FancyTree, self).__init__(nodes)
        self.project_id = project_id

    def get_nested_tree(self, max_depth=None):
        tree = list()
        for source in [n for n in self.graph.nodes() if self.graph.in_degree(n) == 0]:
            tree.append(self._parse_node(source, depth=1, max_depth=max_depth))
        # tree.append({
        #     "key": "0",
        #     "refKey": "0",
        #     "title": "—— Project ——",
        #     "children": list()
        # })
        # for source in [n for n in self.graph.nodes() if self.graph.in_degree(n) == 0]:
        #     tree[0]['children'].append(self._parse_node(source, depth=1, max_depth=max_depth))
        return tree

    def _get_dict_from_node(self, node: 'Union[Specification, SolutionElement]'):
        return {
            # Key can be modified if spec have a parent because fancytree need it to be unique
            "key": f"000_{node.pk}",
            # (Removed because clone extension take care of it and seems to fix reload() problem)
            # Ref key refer to the real pk of the specification
            "refKey": node.pk,
            "title": node.title,
            "children": list()
        }

    def _parse_node(self, node, depth, max_depth, parent_pks=None):
        node_dict = self._get_dict_from_node(self.graph.nodes[node]['data'])
        if parent_pks is None:
            parent_pks = list()
        if len(parent_pks) > 0:
            node_dict['key'] = f"{len(parent_pks):03}_"+"+".join(parent_pks + [str(node_dict['key']), ])
        # (Removed because clone extension take care of it and seems to fix reload() problem)
        if max_depth is None or depth < max_depth:
            for successor in [next_node for next_node in self.graph.successors(node) if
                              'data' in self.graph.nodes[node].keys()]:
                node_dict["children"].append(self._parse_node(successor, depth + 1, max_depth,
                                                              parent_pks=parent_pks + \
                                                                         [str(self.graph.nodes[node]['data'].pk), ]))
        return node_dict


class SpecificationTree(FancyTree):
    included_in = None
    db_nodes = None
    fulfilment_intra = None
    costs = None
    fulfilment = None

    def add_fulfillment_to_tree(self, tree, solution_id):
        self.fulfilment = dict()
        fulfilment_percent = dict((o['specification'], o['fulfill_percent']) for o in
                                  solution.models.Fulfillment.objects.filter(
                                      solution_element__solution=solution_id
                                  ).values('specification').annotate(fulfill_percent=Sum('percent')))
        breach_percent = dict((o['specification'], o['breach_percent']) for o in
                              solution.models.Breach.objects.filter(
                                  solution_element__solution=solution_id
                              ).values('specification').annotate(breach_percent=Sum('percent')))
        self.fulfilment_intra = dict()
        for node in self.graph.nodes():
            self.fulfilment_intra[node] = fulfilment_percent[node] if node in fulfilment_percent.keys() else 0
            if node in breach_percent.keys():
                self.fulfilment_intra[node] -= breach_percent[node]
        # self.fulfilment_percent = dict((o['pk'], o['fulfill_percent']) for o in self.nodes.annotate(
        #     fulfill_percent=Coalesce(
        #         Sum('fulfillment__percent',
        #             filter=Q(
        #                 fulfillment__specification=F('pk'),
        #                 fulfillment__solution_element__solution_id=solution_id)),
        #         0.0) - Coalesce(
        #         Sum('breach__percent',
        #             filter=Q(
        #                 breach__specification=F('pk'),
        #                 breach__solution_element__solution_id=solution_id)
        #             ),
        #         0.0)).values('pk', 'fulfill_percent'))
        self.db_nodes = dict((o.pk, o) for o in self.nodes)
        total_fulfill = 0
        for node in tree:
            fulfilment = self._add_fulfillment(node)
            total_fulfill += fulfilment
            self.fulfilment[node['refKey']] = fulfilment
        return total_fulfill

    def get_queryset(self, nodes: 'QuerySet[Union[Specification]]') -> 'QuerySet[Union[Specification]]':
        qs = super(SpecificationTree, self).get_queryset(nodes)
        return qs.prefetch_related('supervisors').prefetch_related('concerned').prefetch_related(
            Prefetch("tag_set", queryset=Tag.objects.only('id'), to_attr='tags_id')
        ).annotate(
            sum_relative_cost=Sum('included_by__relative_cost')).all()

    def add_supervisors_to_tree(self, tree):
        for node in tree:
            self._add_supervisors_to_tree(node, parent=None)

    def add_concerned_to_tree(self, tree):
        for node in tree:
            self._add_concerned_to_tree(node, parent=None)
            
    def _add_concerned_to_tree(self, node, parent=None):
        node_obj = self.db_nodes[node['refKey']]  # type: Specification
        if not hasattr(node_obj, '_concerned_dict'):
            node_obj._concerned_dict = dict()
        node_dict = node['concerned'] = node_obj._concerned_dict
        if 'parents' not in node_dict.keys():
            node_dict['parents'] = set()
        if len(node_obj.concerned.all()) > 0:
            # If there is one or many direct concerned, set it and remove possible parents_concerned
            node_dict['directs'] = [concerned.name for concerned in node_obj.concerned.all()]
            node_dict['parents'] = set()
        else:
            node_dict['directs'] = list()
            if parent is not None:
                # If there is a parent, find if it has concerned
                parent_dict = self.db_nodes[parent['refKey']]._concerned_dict
                parent_concerned = set()
                if len(parent_dict['directs']) > 0:
                    parent_concerned = set(parent_dict['directs'])
                elif len(parent_dict['parents']) > 0:
                    parent_concerned = parent_dict['parents']
                if len(parent_concerned) > 0:
                    [node_dict['parents'].add(concerned) for concerned in parent_concerned]
        for children in node["children"]:
            self._add_concerned_to_tree(children, parent=node)

    def _add_supervisors_to_tree(self, node, parent=None):
        node_obj = self.db_nodes[node['refKey']]  # type: Specification
        if not hasattr(node_obj, '_supervisors_dict'):
            node_obj._supervisors_dict = dict()
        node_dict = node['supervisors'] = node_obj._supervisors_dict
        if 'parents' not in node_dict.keys():
            node_dict['parents'] = set()
        if len(node_obj.supervisors.all()) > 0:
            # If there is one or many direct supervisors, set it and remove possible parents_supervisors
            node_dict['directs'] = [supervisor.username for supervisor in node_obj.supervisors.all()]
            node_dict['parents'] = set()
            node_dict['conflicts'] = set()
        else:
            node_dict['directs'] = list()
            if 'conflicts' not in node_dict.keys():
                node_dict['conflicts'] = set()
            if parent is not None:
                # If there is a parent, find if it has supervisors
                parent_dict = self.db_nodes[parent['refKey']]._supervisors_dict
                parent_supervisors = set()
                if len(parent_dict['directs']) > 0:
                    parent_supervisors = set(parent_dict['directs'])
                elif len(parent_dict['parents']) > 0:
                    parent_supervisors = parent_dict['parents']
                if len(parent_supervisors) > 0:
                    # If parent have supervisors
                    if len(node_dict['parents']) > 0:
                        # Potential conflicts
                        [node_dict['conflicts'].add(conflict) for conflict in node_dict['parents'] - parent_supervisors]
                        [node_dict['conflicts'].add(conflict) for conflict in parent_supervisors - node_dict['parents']]
                    [node_dict['parents'].add(supervisor) for supervisor in parent_supervisors]
        for children in node["children"]:
            self._add_supervisors_to_tree(children, parent=node)

    def _add_fulfillment(self, node):
        childs_fulfill = 0
        for children in node['children']:
            # Add fulfillment by children
            childs_fulfill += self._add_fulfillment(children)
        if len(node['children']) > 0:
            # Divide by the number of children (to set to 100% only if all childrens are fulfilled)
            childs_fulfill /= len(node['children'])
        node['childs_fulfill'] = childs_fulfill
        # Get node obj from database request in order to access to fulfill_percent
        node_obj = self.db_nodes[node['refKey']]
        node['fulfill'] = self.fulfilment_intra[node['refKey']]
        # Return sum of node fulfill and childs fulfill, cap to 100
        return min(100.0, node['fulfill'] + node['childs_fulfill'])

    def add_cost_to_graph(self):
        # Retrieve all link between nodes with relative_cost
        self.included_in = dict((f"{o.from_specification_id}-{o.to_specification_id}", o) for o in
                                SpecificationIncludedIn.objects.filter(from_specification__in=self.nodes))
        # Init dict to save computed cost
        self.costs = dict()
        nodes_without_parents = [n for n in self.graph.nodes() if self.graph.in_degree(n) == 0]
        # Parse the graph in the BFS order
        for node in networkx.topological_sort(self.graph):
            self._compute_spec_cost(node, root=node in nodes_without_parents)

    def _compute_spec_cost(self, node, root=False):
        spec = self.graph.nodes[node]['data']  # type: Specification
        if root:
            # Node without parent
            self.costs[node] = spec.cost
        else:
            if node not in self.costs.keys():
                self.costs[node] = 0
            for parent in spec.included_in.all():
                self.costs[node] += (self.included_in[f"{node}-{parent.pk}"].relative_cost /
                                     self.graph.nodes[parent.pk]['data'].sum_relative_cost) * self.costs[parent.pk]

    def _get_dict_from_node(self, node: 'Union[Specification, SolutionElement]'):
        node_dict = super(SpecificationTree, self)._get_dict_from_node(node)
        node_dict['tags'] = [tag.id for tag in node.tags_id]
        if self.costs is not None:
            node_dict['cost'] = self.costs[node.pk]
        return node_dict


def add_depth_to_graph(choices):
    if hasattr(choices, 'queryset') is False:
        return choices, None
    parser = ChoicesTree(choices.queryset)
    tree_choices, tree_depth = parser.get_choices_with_depth()
    tree_choices.insert(0, (None, '----'))
    tree_depth.insert(0, 1)
    return tree_choices, tree_depth
