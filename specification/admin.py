from django.contrib import admin
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.shortcuts import render, redirect
from django import forms
from django.utils.translation import gettext_lazy as _
from django.utils.safestring import mark_safe

from django.http.request import HttpRequest

from specification.models import Project, Specification, SpecificationIncludedIn
import specification.forms

from django.db.models.query import QuerySet

# Register your models here.


class IncludedInInline(admin.TabularInline):
    model = SpecificationIncludedIn
    fk_name = "to_specification"
    extra = 1


class SpecificationAdminForm(forms.ModelForm):
    """
    This class is needed because included_in is a m2m relation with a 'through model'
        and django can't manage this automaticly.
    Plus, it allows us to use a FilteredSelectMultiple widget.
    """
    included_in = forms.ModelMultipleChoiceField(
        queryset=Specification.objects.all(),
        required=False,
        widget=FilteredSelectMultiple('included in', False))
    blocked_by = forms.ModelMultipleChoiceField(
        queryset=Specification.objects.all(),
        required=False,
        widget=FilteredSelectMultiple('blocked by', False))

    class Meta:
        model = Specification
        exclude = []


@admin.register(specification.models.Specification)
class SpecificationAdmin(admin.ModelAdmin):
    list_display = ('title', 'is_fulfilled', 'progress', 'is_blocking',)
    ordering = ('is_fulfilled', 'progress', 'is_blocking')
    list_filter = ('is_fulfilled', 'is_blocking')
    list_editable = ('is_fulfilled', 'progress', 'is_blocking')
    actions = ['action_visualize', 'action_visualize_all']
    form = SpecificationAdminForm

    def get_queryset(self, request):
        current_project_id = request.session.get("current_project_id", False)
        qs = specification.models.Specification.objects.none()
        if current_project_id is not False:
            qs = specification.models.Specification.objects.filter(project__pk=current_project_id)
        return qs

    def get_form(self, request, obj=None, change=False, **kwargs):
        form = super(SpecificationAdmin, self).get_form(request, obj, change, **kwargs)
        form.base_fields['project'].initial = Project.objects.get(pk=request.session["current_project_id"])
        form.base_fields['project'].disabled = True
        form.declared_fields['included_in'].queryset = Specification.objects.filter(project__pk=request.session["current_project_id"])
        form.declared_fields['blocked_by'].queryset = Specification.objects.filter(project__pk=request.session["current_project_id"])
        # Reorder fields here (can't reorder in SpecificationAdminForm, don't know why)
        form.base_fields = {
            'project': form.base_fields['project'],
            'title': form.base_fields['title'],
            'is_fulfilled': form.base_fields['is_fulfilled'],
            'progress': form.base_fields['progress'],
            'included_in': form.declared_fields['included_in'],
            'description': form.base_fields['description'],
            'is_blocking': form.base_fields['is_blocking'],
            'blocked_by': form.declared_fields['blocked_by'],
        }
        return form

    @admin.action(description=_("Visualize those specifications"))
    def action_visualize(self, request, queryset):
        specs = specification.models.Specification.objects.filter(pk__in=queryset.values_list('pk', flat=True))
        form = specification.forms.VisualizeSpecsAdminForm(specs, select_all=False)
        return render(request, "specification/admin/admin_visualize_form.html", context={"form": form})

    @admin.action(description=_("Visualize all specifications"))
    def action_visualize_all(self, request, queryset):
        specs = specification.models.Specification.objects.filter(project__pk=request.session["current_project_id"])
        form = specification.forms.VisualizeSpecsAdminForm(specs, select_all=True)
        return render(request, "specification/admin/admin_visualize_form.html", context={"form": form, "select_all": True})


@admin.register(specification.models.Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name', )
    actions = ['action_activate', ]

    @admin.action(description=_("Activate this project"))
    def action_activate(self, request: HttpRequest, queryset: 'QuerySet[Project]'):
        if len(queryset) != 1:
            raise ValueError(_("Should select one and only one project to activate"))
        queryset[0].set_active_for_session(request.session)
        return redirect(request.get_full_path())


@admin.register(specification.models.UserPreferences)
class UserPreferencesAdmin(admin.ModelAdmin):
    pass


@admin.register(specification.models.Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ('name_badge', 'project')
    list_filter = ('project', )

    def name_badge(self, obj):
        return mark_safe(f"<span class='badge' style='border-radius: 0.375rem; padding: 0.35em 0.65em;background-color: {obj.color}; color: {obj.text_color}'>{obj.name}</span>")


@admin.register(specification.models.Actor)
class ActorAdmin(admin.ModelAdmin):
    list_display = ('name', 'project')
    list_filter = ('project', )
