from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils.translation import gettext
from specification.utils import f_string_eval as f
from django.contrib.auth.models import User

import pgtrigger
from colorfield.fields import ColorField


class Specification(models.Model):
    project = models.ForeignKey('specification.Project', null=False, blank=False, on_delete=models.CASCADE,
                                verbose_name=_("project"))
    title = models.CharField(max_length=256, verbose_name=_("title"))
    is_blocking = models.BooleanField(default=True, verbose_name=_("is blocking"))
    is_fulfilled = models.BooleanField(default=False, verbose_name=_("is fulfilled"))
    progress = models.FloatField(default=0.0, verbose_name=_("progress"))
    # Through model needed for trigger to be fired on includes changes
    included_in = models.ManyToManyField('specification.Specification', blank=True,
                                         symmetrical=False,
                                         through='SpecificationIncludedIn',
                                         through_fields=(
                                             "from_specification", "to_specification"),
                                         verbose_name=_("included in")
                                         )
    blocked_by = models.ManyToManyField('specification.Specification', related_name='blocking_for', blank=True,
                                        verbose_name=_("blocked by"))
    description = models.TextField(blank=True, verbose_name=_("description"))
    supervisors = models.ManyToManyField(User, blank=True, verbose_name=_("supervisor(s)"))
    concerned = models.ManyToManyField('Actor', blank=True, verbose_name=_("concerned"))
    cost = models.FloatField(_("intrinsic cost"), default=0.0)

    class Meta:
        verbose_name = _("specification")
        verbose_name_plural = _("specifications")

    def __repr__(self):
        return self.title

    def __str__(self):
        return self.title


class SpecificationIncludedIn(models.Model):
    from_specification = models.ForeignKey('Specification', on_delete=models.CASCADE,
                                           verbose_name=_("included specification"))
    to_specification = models.ForeignKey('Specification', on_delete=models.CASCADE, related_name='included_by',
                                         verbose_name=_("parent specification"))
    relative_cost = models.FloatField(_("relative cost"), default=1.0)

    class Meta:
        db_table = "specification_specification_included_in"
        unique_together = (('from_specification', 'to_specification'))
        verbose_name = _("link between specifications")
        triggers = [
            pgtrigger.Trigger(
                name="check_no_cycle",
                operation=pgtrigger.Insert | pgtrigger.Update,
                timing=pgtrigger.Deferred,
                when=pgtrigger.After,
                level=pgtrigger.Row,
                func='''WITH RECURSIVE graph AS (
    SELECT from_specification_id AS id
        , ARRAY[to_specification_id, from_specification_id] AS path
        , (from_specification_id = to_specification_id) AS cycle    -- first step could be circular
    FROM  specification_specification_included_in spec_inc

    UNION ALL
    SELECT spec_inc.from_specification_id, spec_inc.to_specification_id || path, spec_inc.to_specification_id = ANY(path)
    FROM   graph g
    JOIN   specification_specification_included_in spec_inc ON spec_inc.from_specification_id = g.path[1]
    WHERE  NOT g.cycle
    )
    SELECT COUNT(id) INTO n_cycles
    FROM   graph
    WHERE  cycle;
    IF n_cycles > 0 THEN
    RAISE EXCEPTION
                    'pgtrigger: Circular reference detected in %',
                    TG_TABLE_NAME;
    ELSE
    RETURN NEW;
    END IF;''',
                declare=[('n_cycles', 'INTEGER'), ]
            )
        ]


class Project(models.Model):
    name = models.CharField(max_length=256, verbose_name=_("name"))
    users = models.ManyToManyField('auth.User', blank=True, verbose_name=_("users"))

    class Meta:
        verbose_name = _("project")
        verbose_name_plural = _("projects")

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.name

    def set_active_for_session(self, session):
        session['current_project_id'] = self.pk
        session['current_project_name'] = self.name
        if self.solutionset_set.count() > 0:
            self.solutionset_set.first().set_active_for_session(session)
        else:
            session['current_solution_name'] = None
            session['current_solution_id'] = None


class UserPreferences(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="preferences", verbose_name=_("user"))
    default_project = models.ForeignKey('specification.Project', on_delete=models.CASCADE,
                                        verbose_name=_("default project"))
    show_solutions = models.BooleanField(verbose_name=_("show solutions"),
                                         help_text=_("Display solution panel or not"), default=True)
    language = models.CharField(max_length=32, default='en', verbose_name=_("language"))

    class Meta:
        verbose_name = _("user preferences")
        verbose_name_plural = _("users preferences")

    def __repr__(self):
        return f(gettext("Preferences of {self.user.username}"))

    def __str__(self):
        return f(gettext("Preferences of {self.user.username}"))


class Tag(models.Model):
    COLOR_PALETTE = [
        ("#FFFFFF", "white",),
        ("#000000", "black",),
        ("#dda000", "orange",),
        ("#ff0000", "red",),
        ("#0000ff", "blue",),
        ("#00ff00", "green",),
    ]
    project = models.ForeignKey('specification.Project', null=False, blank=False, on_delete=models.CASCADE,
                                verbose_name=_("project"))
    name = models.CharField(max_length=128, unique=True)
    color = ColorField(format="hex", samples=COLOR_PALETTE, default="#dda000")
    text_color = ColorField(format="hex", samples=[
        ("#FFFFFF", "white",), ("#000000", "black",)], default="#000000")
    description = models.TextField(blank=True)
    specifications = models.ManyToManyField('specification.Specification', blank=True)

    class Meta:
        constraints = [models.UniqueConstraint(
            fields=["project", "name"],
            name="unique_tag_name_per_project")
        ]

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.name


class Actor(models.Model):
    name = models.CharField(verbose_name=_("name"), max_length=128)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)

    class Meta:
        constraints = [models.UniqueConstraint(
            fields=["project", "name"],
            name="unique_actor_name_per_project")
        ]

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.name
