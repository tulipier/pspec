from django.forms.widgets import SelectMultiple


class MultiSlimSelect(SelectMultiple):
    template_name = "slimselect/slimselect.html"

    def __init__(self, attrs=None, choices=()):
        _attrs = {
            'allowDeselectOption': True,
            'showSearch': False
        }
        if attrs is not None:
            _attrs.update(attrs)
        super(MultiSlimSelect, self).__init__(attrs=_attrs, choices=choices)

    class Media:
        css = {
            'all': ('css/slimselect/slimselect.min.css',)
        }
        js = ('js/slimselect/slimselect.min.js',)
