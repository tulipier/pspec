from django.apps import AppConfig


class SlimselectConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "slimselect"
