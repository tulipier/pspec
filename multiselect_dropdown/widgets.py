from django.forms.widgets import SelectMultiple


class MultiSelectDropdown(SelectMultiple):
    template_name = "multiselect_dropdown/multiselect_dropdown.html"
    class Media:
        css = {
            'all': ('css/multiselect-dropdown/multiselect-dropdown.css',)
        }
        js = ('js/multiselect-dropdown/multiselect-dropdown.js',)
