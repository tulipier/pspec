from django.apps import AppConfig


class MultiselectDropdownConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "multiselect_dropdown"
